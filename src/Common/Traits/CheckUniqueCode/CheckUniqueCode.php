<?php


namespace App\Common\Traits\CheckUniqueCode;


use App\Entity\Club;
use App\Entity\User;
use App\Service\CodeGenerator\ClubCodeGenerator;
use App\Service\CodeGenerator\UserCodeGenerator;
use Doctrine\Common\Persistence\ObjectManager;

trait CheckUniqueCode
{
    private $em;
    private $userCodeGenerator;
    private $clubCodeGenerator;

    public function __construct(ObjectManager $em, UserCodeGenerator $userCodeGenerator, ClubCodeGenerator $clubCodeGenerator)
    {
        $this->em = $em;
        $this->userCodeGenerator = $userCodeGenerator;
        $this->clubCodeGenerator = $clubCodeGenerator;
    }

    public function generateNewClubCodeIfAlreadyExists($clubCode)
    {
        $club = $this->em->getRepository(Club::class)->findOneBy(['code' => $clubCode]);
        while ($club) {
            $clubCode = $this->clubCodeGenerator->establishmentCodeGenerator();
            $club = $this->em->getRepository(Club::class)->findOneBy(['code' => $clubCode]);
        }
        return $clubCode;
    }

    public function generateNewUserCodeIfAlreadyExists($clubCode, $userCode)
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['userCode' => $userCode, 'club' => $clubCode]);
        while ($user) {
            $userCode = $this->userCodeGenerator->userCodeGenerator();
            $user = $this->em->getRepository(User::class)->findOneBy(['userCode' => 'club', 'club' => $clubCode]);
        }
        return $userCode;
    }


}