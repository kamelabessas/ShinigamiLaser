<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180423083500 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE card (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, point INT DEFAULT NULL, date_create DATETIME NOT NULL, nb_card_create INT NOT NULL, card_code VARCHAR(64) NOT NULL, INDEX IDX_161498D3A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE club (id INT AUTO_INCREMENT NOT NULL, establishment_name VARCHAR(50) NOT NULL, code INT NOT NULL, address VARCHAR(250) NOT NULL, UNIQUE INDEX UNIQ_B8EE387277153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, category VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, price DOUBLE PRECISION NOT NULL, img VARCHAR(255) DEFAULT NULL, slug VARCHAR(128) NOT NULL, UNIQUE INDEX UNIQ_232B318C989D9B62 (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE club_game (game_id INT NOT NULL, club_id INT NOT NULL, INDEX IDX_47E3FB6CE48FD905 (game_id), INDEX IDX_47E3FB6C61190A32 (club_id), PRIMARY KEY(game_id, club_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matche (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, date DATETIME NOT NULL, duration INT NOT NULL, min_nb_player INT NOT NULL, max_nb_player INT NOT NULL, INDEX IDX_9FCAD510E48FD905 (game_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE match_player (id INT AUTO_INCREMENT NOT NULL, match_id INT DEFAULT NULL, user_id INT DEFAULT NULL, score INT NOT NULL, created_date DATETIME NOT NULL, end_date DATETIME DEFAULT NULL, is_played TINYINT(1) NOT NULL, INDEX IDX_397683642ABEACD6 (match_id), INDEX IDX_39768364A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newsletter (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, club_id INT DEFAULT NULL, name VARCHAR(20) NOT NULL, username VARCHAR(20) NOT NULL, email VARCHAR(255) NOT NULL, birthdate DATETIME NOT NULL, password VARCHAR(64) NOT NULL, address VARCHAR(64) NOT NULL, user_code INT DEFAULT NULL, token VARCHAR(64) DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', service VARCHAR(180) DEFAULT NULL, picture VARCHAR(180) DEFAULT NULL, picture_name VARCHAR(255) DEFAULT NULL, status INT NOT NULL, UNIQUE INDEX UNIQ_8D93D6495F37A13B (token), INDEX IDX_8D93D64961190A32 (club_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE card ADD CONSTRAINT FK_161498D3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE club_game ADD CONSTRAINT FK_47E3FB6CE48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE club_game ADD CONSTRAINT FK_47E3FB6C61190A32 FOREIGN KEY (club_id) REFERENCES club (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE matche ADD CONSTRAINT FK_9FCAD510E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE match_player ADD CONSTRAINT FK_397683642ABEACD6 FOREIGN KEY (match_id) REFERENCES matche (id)');
        $this->addSql('ALTER TABLE match_player ADD CONSTRAINT FK_39768364A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64961190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE club_game DROP FOREIGN KEY FK_47E3FB6C61190A32');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64961190A32');
        $this->addSql('ALTER TABLE club_game DROP FOREIGN KEY FK_47E3FB6CE48FD905');
        $this->addSql('ALTER TABLE matche DROP FOREIGN KEY FK_9FCAD510E48FD905');
        $this->addSql('ALTER TABLE match_player DROP FOREIGN KEY FK_397683642ABEACD6');
        $this->addSql('ALTER TABLE card DROP FOREIGN KEY FK_161498D3A76ED395');
        $this->addSql('ALTER TABLE match_player DROP FOREIGN KEY FK_39768364A76ED395');
        $this->addSql('DROP TABLE card');
        $this->addSql('DROP TABLE club');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE club_game');
        $this->addSql('DROP TABLE matche');
        $this->addSql('DROP TABLE match_player');
        $this->addSql('DROP TABLE newsletter');
        $this->addSql('DROP TABLE user');
    }
}
