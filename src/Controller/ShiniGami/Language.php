<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 18/04/2018
 * Time: 15:17
 */

namespace App\Controller\ShiniGami;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouteCollectionBuilder;

class Language extends Controller
{
    /**
     * Change the locale for the current user
     *
     * @param String $language
     * @param Request $request
     * @return RedirectResponse
     * @Route("/setlocale/{language}", name="setlocale")
     * @Template()
     */
    public function setLocaleAction($language = null, Request $request)
    {
        if($language != null)
        {
            // On enregistre la langue en session
            $this->get('session')->set('_locale', $language);
        }

        // on tente de rediriger vers la page d'origine
        $url = $request->headers->get('referer');
        if(empty($url))
        {
            $url = $this->router->generate('index');
        }

        return new RedirectResponse($url);
    }

}