<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 06/04/2018
 * Time: 19:02
 */

namespace App\Controller\ShiniGami;

use App\Entity\Matche;
use App\Entity\MatchPlayer;
use App\Form\MatcheType;
use App\Service\Match\ScoreGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MatchController extends Controller
{
    /**
     * Show matches list
     *
     * @Route(
     *     path="/match",
     *     name="index_match"
     *      )
     * @Method({"GET", "POST"})
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $matches = $em->getRepository(Matche::class)->findAll();
        return $this->render("match/index.html.twig", [
            "matches" => $matches
        ]);
    }

    /**
     * Show one match
     *
     * @Route(path="/match/show-match/{id}", name="show_match")
     * @param Matche $match
     * @return Response
     * @internal param Request $request
     */
    public function showAction(Matche $match)
    {
        return $this->render("match/show_match.html.twig", [
            "match" => $match
        ]);
    }

    /**
     * Add a new match
     *
     * @Route(path="/match/add-match", name="add_match")
     * @param Request $request
     * @return Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addAction(Request $request)
    {
        $match = new Matche();
        $form = $this->createForm(MatcheType::class, $match);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($match);
            $em->flush();
            return $this->redirectToRoute("index_match");
        }
        return $this->render("match/add_match.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * Update an match
     *
     * @Route(path="/match/edit-match/{id}", name="edit_match")
     * @param Request $request
     * @param Matche $match
     * @return Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, Matche $match)
    {
        $form = $this->createForm(MatcheType::class, $match);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($match);
            $em->flush();
            return $this->redirectToRoute("index_match");
        }

        return $this->render("match/add_match.html.twig", [
            "form" => $form->createView(),
            "id" => $match->getId()
        ]);
    }

    /**
     * Delete an match
     *
     * @Route(path="/match/delete-match/{id}", name="delete_match")
     * @param Request $request
     * @param Matche $match
     * @Method("POST")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, Matche $match)
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('index_match');
        }
        $matchName = $match->getName();

        $em = $this->getDoctrine()->getManager();
        $em->remove($match);
        $em->flush();

        $this->addFlash('info', "La partie $matchName a bien été supprimée.");

        return $this->redirectToRoute('index_match');
    }

    /**
     * @Route(path="/match/list-scores/{id}", name="list_scores_action" )
     * @param MatchPlayer $matchPlayer
     * @param ScoreGenerator $scoreGenerator
     * @ParamConverter("MatchPlayer", options={"mapping"={"id"="id"}})
     * @return Response
     */
    public function listScoresAction(MatchPlayer $matchPlayer, ScoreGenerator $scoreGenerator)
    {
        if($matchPlayer->getIsPlayed() == 0) {
            try {
                $scoreGenerator->generateScoreToPlayer($matchPlayer);

                $this->addFlash('flash_key',"Add done!");

            } catch (\Exception $e) {
                $this->addFlash('flash_key',"Add not done: " . $e->getMessage());
            }
        }
        $matchPlayers = $this->getDoctrine()
            ->getRepository(MatchPlayer::class)
            ->findScoresButForThisMP($this->getUser(), $matchPlayer);
        return $this->render('match/list_scores.html.twig', [
            'matchPlayer' => $matchPlayer,
            'matchPlayers' => $matchPlayers
        ]);
    }

    /**
     * @Route(path="/match/matches-to-play", name="matches_to_play_action")
     */
    public function listMatchesToPlay()
    {
        $matchPlayers = $this->getDoctrine()
            ->getRepository(MatchPlayer::class)
            ->findMatchPlayersWhereIsNotPlayed($this->getUser());

        return $this->render('match/list_matches_to_play.html.twig', [
           'matchPlayers' => $matchPlayers
        ]);
    }
}