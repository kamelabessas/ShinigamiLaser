<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 30/03/2018
 * Time: 11:10
 */

namespace App\Controller\ShiniGami;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class indexController extends Controller
{

    /**
     * @Route(path="/{_locale}",
     *     name="index",
     *     defaults={"_locale"="fr"})
     */
    public function indexAction()
    {
        return $this->render("index/index.html.twig");
    }
}