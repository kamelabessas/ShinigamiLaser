<?php

namespace App\Controller\ShiniGami;

use App\Common\Traits\CheckUniqueCode\CheckUniqueCode;
use App\Entity\Club;
use App\Form\ClubType;
use App\Service\CodeGenerator\ClubCodeGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class ClubController
 * @package App\Controller\Club
 */
class ClubController extends Controller
{
    use CheckUniqueCode;
    /**
     * Show establishments list
     *
     * @Route(
     *     path="/{_locale}/club",
     *     name="index_establishment",
     *     defaults={"_locale"="fr"}
     *      )
     * @Method({"GET", "POST"})
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $establishments = $em->getRepository(Club::class)->findAll();
        return $this->render("club/index.html.twig", [
            "establishments" => $establishments
        ]);
    }

    /**
     * Show one establishment
     *
     * @Route(path="/club/show-establishment/{id}", name="show_establishment")
     * @param Club $establishment
     * @return Response
     * @internal param Request $request
     */
    public function showAction(Club $establishment)
    {
        return $this->render("club/show_establishment.html.twig", [
            "establishment" => $establishment
        ]);
    }

    /**
     * Add a new establishment
     *
     * @Route(path="/club/add-establishment", name="add_establishment")
     * @param Request $request
     * @param ClubCodeGenerator $codeGenerator
     * @return Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addAction(Request $request, ClubCodeGenerator $codeGenerator)
    {
        $club = new Club();
        $form = $this->createForm(ClubType::class, $club);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $codeClub = $codeGenerator->establishmentCodeGenerator();
            $code = $this->generateNewClubCodeIfAlreadyExists($codeClub);
            $club->setCode($code);

            $establishmentName = $request->request->get('club')['establishmentName'];

            $em = $this->getDoctrine()->getManager();
            $em->persist($club);
            $em->flush();

            $this->addFlash('success', "L'établissement '$establishmentName' a bien été ajouté.");
            return $this->redirectToRoute("index_establishment");
        }
        return $this->render("club/add_establishment.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * Update an establishment
     *
     * @Route(path="/club/edit-establishment/{id}", name="edit_establishment")
     * @param Request $request
     * @param Club $club
     * @return Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, Club $club)
    {
        $form = $this->createForm(ClubType::class, $club);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($club);
            $em->flush();
            return $this->redirectToRoute("index_establishment");
        }

        return $this->render("club/add_establishment.html.twig", [
            "form" => $form->createView(),
            "id" => $club->getId()
        ]);
    }

    /**
     * Delete an establishment
     *
     * @Route(path="/club/delete-establishment/{id}", name="delete_establishment")
     * @param Request $request
     * @param Club $club
     * @Method("POST")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, Club $club)
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('index_establishment');
        }
        $establishmentName = $club->getEstablishmentName();

        $em = $this->getDoctrine()->getManager();
        $em->remove($club);
        $em->flush();

        $this->addFlash('info', "L'établissement $establishmentName a bien été supprimé.");

        return $this->redirectToRoute('index_establishment');
    }

}

