<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 06/04/2018
 * Time: 16:57
 */

namespace App\Controller\ShiniGami\Card;


use App\Common\Traits\CheckUniqueCode\CheckUniqueCode;
use App\Entity\Card;
use App\Entity\User;
use App\Form\CardType;
use App\Form\EditCardType;
use App\Service\CodeGenerator\CardCodeGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CardController
 * @package App\Controller\Card
 */
class CardController extends Controller
{

    use CheckUniqueCode;

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/card/list-cards", name="list_cards")
     */
    public function listCards()
    {

        $em = $this->getDoctrine()->getManager();
        $cards = $em->getRepository(Card::class)->findAll();
        return $this->render("card/list_cards.html.twig", [
            "cards" => $cards
        ]);

    }

    /**
     * @param Card $card
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/card/show-card/{id}", name="show_card")
     */
    public function showCard(Card $card)
    {
        return $this->render('card/show_card.html.twig', [
            'card' => $card,
        ]);
    }

    /**
     * @Route("/card/create-card/{id}", name="create_card")
     * @param Request $request
     * @param CardCodeGenerator $cardCode
     * @param User $user
     * @return Response
     */
    public function createCard(Request $request, CardCodeGenerator $cardCode, User $user)
    {

        $card = new Card();
        $card->setUser($user);
        $codeUser = $user->getUserCode();
        $club = $user->getClub();
        $clubCode = $club->getCode();
        $card->setCardCode($cardCode->cardCodeGenerator($clubCode, $codeUser));
        $form = $this->createForm(CardType::class, $card);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($card);
            $em->flush();
            $this->addFlash(
                'notice',
                'La carte a été bien crée'
            );


            return $this->redirectToRoute("list_cards");
        }

        return $this->render("card/create_card.html.twig", [
            "form" => $form->createView(),
            'user' => $user

        ]);

    }

    /**
     * @param Request $request
     * @param Card $card
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/card/edit-card/{id}", name="edit_card")
     */
    public function editCard(Request $request, Card $card)
    {
        $form = $this->createForm(EditCardType::class, $card);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($card);
            $em->flush();

            $this->addFlash(
                'notice',
                'La carte est bien à jour'
            );

            return $this->redirectToRoute('list_cards');
        }
        return $this->render('card/edit_card.html.twig', [
            "form" => $form->createView(),
        ]);

    }

    /**
     * @Method("POST")
     * @Route("/card/delete-card/{id}", name="delete_card")
     * @param Request $request
     * @param Card $card
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     */
    public function deleteCard(Request $request, Card $card)
    {

        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('list_cards');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($card);
        $em->flush();
        $this->addFlash(
            'success',
            'la carte a bien été supprimée');

        return $this->redirectToRoute('list_cards');


    }


}