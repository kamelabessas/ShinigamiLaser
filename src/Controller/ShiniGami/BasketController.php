<?php
/**
 * Created by PhpStorm.
 * User: Mitra
 * Date: 07/04/2018
 * Time: 20:44
 */

namespace App\Controller\ShiniGami;

use App\Entity\Matche;
use App\Entity\User;
use App\Repository\MatcheRepository;
use App\Service\Match\PersistPurchasedGames;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Stripe\Charge;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BasketController extends Controller
{

    /**
     * @Route(path="/participate/{id}", name="participate_action")
     * @param Request $request
     * @param Matche $match
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function participateAction(Request $request, Matche $match)
    {
        $id = $match->getId();

        $session = $request->getSession();

        if (!$session->has('basket')) {
            $session->set('basket', array());
        }
        $basket = $session->get('basket');
        $qte = $request->query->get('qte');
        if ($qte != null && $qte >= $match->getMinNbPlayer() && $qte <= $match->getMaxNbPlayer()) {
            $basket[$id] = $qte;
        } else {
            $basket[$id] = $match->getMinNbPlayer();
        }
        $session->set('basket', $basket);
        return $this->redirectToRoute('basket_action');
    }

    /**
     * @Route(path="basket", name="basket_action")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function basketAction(Request $request)
    {
        $session = $request->getSession();
        if (!$session->has('basket')) {
            $session->set('basket', array());
        }
        /** @var MatcheRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Matche::class);
        $matches = $repository->findMatchesByIdsInBasket(array_keys($session->get('basket')));

        return $this->render('/basket/userBasket.html.twig', [
            'matches' => $matches,
            'basket' => $session->get('basket')
        ]);
    }

    /**
     * @Route(path="validation", name="validation_action")
     * @param Request $request
     * @param PersistPurchasedGames $persistPurchasedGames
     * @Security("has_role('ROLE_USER')")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function basketValidationAction(Request $request, PersistPurchasedGames $persistPurchasedGames)
    {
        //$request->getSession()->remove('basket');
        $session = $request->getSession();
        if ($session->has('basket')) {
            $matchIds = array_keys($session->get('basket'));
            /** @var User $user */
            $user = $this->getUser();

            try {
                Stripe::setApiKey("sk_test_Ucsbo522xjMGd4ydLOm2gxAW");
                Charge::create(array(
                    "amount" => $session->get('totalHT')*100,
                    "currency" => "eur",
                    "source" => $request->request->get('stripeToken'), // obtained with Stripe.js
                    "description" => "desc"
                ));
            }catch(\Exception $e) {
                $this->addFlash('error', $e->getMessage());

                return $this->redirectToRoute('payment_form_action');
            }

            $checkInsertResult = $persistPurchasedGames->insertPurchasedGamesInDb($matchIds, $user);
            if ($checkInsertResult) {
                $request->getSession()->remove('basket');
            }

            return $this->redirectToRoute('matches_to_play_action');
        }

        return $this->redirectToRoute('basket_action');
    }

    /**
     * @Route(path="/basket-delete/{id}", name="basket_delete")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $session = $request->getSession();

        $basket = $session->get('basket');
        if (array_key_exists($id, $basket)) {
            unset($basket[$id]);
            $session->set('basket', $basket);
        }

        return $this->redirectToRoute('basket_action');
    }

    /**
     * Show Payment form
     *
     * @Route(
     *     path="/basket/payment-form",
     *     name="payment_form_action"
     *      )
     *
     */
    public function paymentFormAction(Request $request)
    {
        return $this->render("payment/payment.html.twig");
    }
}