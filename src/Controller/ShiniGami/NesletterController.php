<?php
/**
 * Created by PhpStorm.
 * User: haroun
 * Date: 10/04/2018
 * Time: 20:56
 */

namespace App\Controller\ShiniGami;


use App\Form\NewsletterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NesletterController extends Controller
{

    /**
     * @return mixed
     */
    public function newsletter() {
        # Création du Formulaire
        $form = $this->createForm(NewsletterType::class);

        # Affichage du Formulaire Newsletter
        return $this->render('newsletter/newsletter.html.twig',[
            'form' => $form->createView()
        ]);
    }
}