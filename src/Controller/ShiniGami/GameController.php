<?php

namespace App\Controller\ShiniGami;

use App\Entity\Game;
use App\Form\GameType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GameController
 * @package App\Controller\Game
 */
class GameController extends Controller
{

    /**
     * Show games list
     *
     * @Route(
     *     path="/game",
     *     name="index_game"
     *      )
     * @Method({"GET", "POST"})     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $games = $em->getRepository(Game::class)->findAll();
        // We want just to be able delete the games that haven't matches :
        //$gamesHaveNotMatches = $em->getRepository(Game::class)->findGamesHaveNotMatches();
        return $this->render("game/index.html.twig", [
            "games" => $games
        ]);
    }

    /**
     * @Route("/game/show-game/{slug}", name="show_game")
     * @ParamConverter("game", options={"mapping": {"slug": "slug"}})
     * @param Game $game
     * @return Response
     */
    public function showAction(Game $game)
    {
        return $this->render("game/show_game.html.twig", [
            "game" => $game
        ]);
    }

    /**
     * Add a new game
     *
     * @Route(path="/game/add-game", name="add_game")
     * @param Request $request
     * @return Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addAction(Request $request)
    {
        $game = new Game();
        $form = $this->createForm(GameType::class, $game);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $img = $game->getImg();

            if(null !== $img){

                $fileName = $game->addSlugToImageName() . '.' . $img->guessExtension();
                $img->move(
                    $this->getParameter('game_assets_dir'),
                    $fileName
                );
                $game->setImg($fileName);
            }

            $em->persist($game);
            $em->flush();


            return $this->redirectToRoute("index_game");
        }
        return $this->render("game/add_game.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * Update an game
     *
     * @Route(path="/game/edit-game/{id}", name="edit_game")
     * @param Request $request
     * @param Game $game
     * @param Packages $asset
     * @return Response
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function updateAction(Request $request, Game $game, Packages $asset)
    {
        $form = $this->createForm(GameType::class, $game);
        $form->add('img', FileType::class, [
            'data_class' => null,
            'attr' =>[
                'data-default-file' => $asset->getUrl('images/game/'.$game->getImg()),
                'class' => 'form-control dropify',
            ]
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $image = $game->getImg();
            # Only if image exist
            if ($image) {
                $fileName = $game->addSlugToImageName() . '.' . $image->guessExtension();

                # move uploaded file
                $image->move(
                    $this->getParameter('game_assets_dir'),
                    $fileName
                );

                $game->setImg($fileName);
            } else {
                # restore original image if not modified
                $game->setImg($image);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($game);
            $em->flush();

            return $this->redirectToRoute("index_game");
        }

        return $this->render("game/add_game.html.twig", [
            "form" => $form->createView(),
            "id"   => $game->getId()
        ]);
    }

    /**
     * Delete an game
     *
     * @Route(path="/game/delete-game/{id}", name="delete_game")
     * @param Request $request
     * @param Game $game
     * @Method("POST")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, Game $game)
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute("index_game");
        }
        $gameName = $game->getName();

        $em = $this->getDoctrine()->getManager();
        $em->remove($game);
        $em->flush();

        $this->addFlash('info', "Le jeu $gameName a bien été supprimé.");

        return $this->redirectToRoute('index_game');
    }
}