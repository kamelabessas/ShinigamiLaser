<?php
/**
 * Created by PhpStorm.
 * User: haroun
 * Date: 28/03/2018
 * Time: 15:26
 */

namespace App\Controller\Security;


use App\Common\Traits\CheckUniqueCode\CheckUniqueCode;
use App\Entity\User;
use App\Events\Events;
use App\Form\UserType;
use App\Service\CodeGenerator\UserCodeGenerator;
use App\Service\Token\GenerateToken;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends Controller
{

    use CheckUniqueCode;


    /**
     * Inscription d'un User
     * @Route("/register", name="register", methods={"GET","POST"})
     */

    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, EventDispatcherInterface $eventDispatcher, GenerateToken $token)
    {

        $user = new User();
        $user->setRoles('ROLE_USER');
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            // Par defaut l'User aura toujours le rôle ROLE_USER
            $user->setToken($token->generateToken());
            $user->setInactive();

            // On enregistre l'User dans la base
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            //On déclenche l'event
            $event = new GenericEvent($user);
            $eventDispatcher->dispatch(Events::USER_REGISTERED, $event);


            $this->addFlash('success', 'Bienvenue dans Shinigami laser, vous venez de recevoir un email d\'activation de compte');


            # Redirection sur la page de connexion
            return $this->redirectToRoute('login');

        }

        return $this->render('security/register.html.twig', array('form' => $form->createView()));
    }


    /**
     * @Route("/activate-account/{token}", name="activate-account")
     * @Method={"POST"}
     * @param Request $request
     * @param UserCodeGenerator $userCode
     * @param $token
     * @return Response
     */

    public function activateAccount(Request $request, UserCodeGenerator $userCode, $token)
    {

        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneByToken($token);

        if (!$user) {

            return $this->redirectToRoute('register', ['register' => 'notfound']);
        }
        if ($user->isEnabled()) {
            return $this->redirectToRoute('login', ['register' => 'actived']);
        }
        # checkif author is banned
        if ($user->isBanned()) {
            return $this->redirectToRoute('login', ['register' => 'banned']);
        }
        # checkif author is banned
        if ($user->isClosed()) {
            return $this->redirectToRoute('login', ['register' => 'closed']);
        }

            $user->setToken(null);
            $user->setActive();
            $userCode = $userCode->userCodeGenerator();
            $club = $user->getClub();
            $clubCode = $club->getCode();
            $userCode = $this->generateNewUserCodeIfAlreadyExists($userCode, $clubCode);
            $user->setUserCode($userCode);
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'votre compte est valide vous pouvez vous connecter');

            return $this->redirectToRoute('login');

    }


    /**
     * Connexion d'un User
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('index');

        }
        # Récupération du message d'erreur s'il y en a un.
        $error = $authenticationUtils->getLastAuthenticationError();

        # Dernier email saisie par l'User.
        $lastEmail = $authenticationUtils->getLastUsername();
        # Affichage du Formulaire
        return $this->render('security/login.html.twig', array(
            'last_email' => $lastEmail,
            'error' => $error,
        ));
    }


}
