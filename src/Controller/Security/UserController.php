<?php

namespace App\Controller\Security;

use App\Common\Traits\String\SlugifyTrait;
use App\Entity\User;
use App\Form\EditUserByAdminType;
use App\Form\EditUserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class UserController extends Controller
{

    use SlugifyTrait;


    /**
     * @Route("/user/list-users", name="list_users")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listUsers()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();
        return $this->render("user/list_users.html.twig", [

            "users" => $users
        ]);

    }

    /**
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/user/show-user/{id}", name="show_user")
     */
    public function showUser(User $user)
    {
        return $this->render('user/show_user.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/user/edit-user/{id}", name="edit_user")
     */
    public function editUserByUser(Request $request, User $user)
    {
        $form = $this->createForm(EditUserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();
            $picture = $user->getPicture();

            if (null !== $picture) {

                $pictureName = $this->slugify($user->getPictureName()) . '.' .
                    $picture->guessExtension();
                $picture->move(

                    $this->getParameter('user_assets_dir'),
                    $pictureName
                );

                $user->setPicture($pictureName);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

        }

        return $this->render('user/edit_user_profile.html.twig', array(
            'form' => $form->createView(),
            'user' => $user
        ));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/user/edit-user-by-admin/{id}", name="edit_user_by_admin")
     */
    public function editUserByAdmin(Request $request, User $user)
    {

        $form = $this->createForm(EditUserByAdminType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            switch ($user->getService()) {
                case 'Marketing':
                    $user->setRoles('ROLE_MARKETING');
                    break;
                case 'Quality_Manager':
                    $user->setRoles('ROLE_QUALITY_MANAGER');
                    break;
                case 'Admin':
                    $user->setRoles('ROLE_ADMIN');
                    break;
                case 'Super_Admin':
                    $user->setRoles('ROLE_SUPER_ADMIN');
                    break;
            }


            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }

        return $this->render('user/edit_user_by_admin.html.twig', array(
            'form' => $form->createView(),
            'user' => $user
        ));
    }


    /**
     * @Route(path="/user/delete-user/{id}", name="delete_user")
     * @param Request $request
     * @param  $user
     * @Method("POST")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */

    public function deleteUser(Request $request, User $user)
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('list_users');
        }
        $username = $user->getUsername();
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        $this->addFlash(
            'info',
            'l\'utilisateur' . $username . 'a bien été supprimé');

        return $this->redirectToRoute('list_users');


    }

}
