<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 06/04/2018
 * Time: 10:58
 */

namespace App\Controller\Security;


use App\Entity\User;
use App\Events\Events;
use App\Form\ResetPasswordType;
use App\Form\SendMailType;
use App\Service\Token\GenerateToken;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * Class ChangePasswordController
 * @package App\Controller\Security
 */
class ChangePasswordController extends Controller
{

    /**
     * @Route("/send-mail", name="send_mail")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param EventDispatcherInterface $eventDispatcher
     * @param GenerateToken $token
     * @return Response
     */
    public function sendMailAction(Request $request, EventDispatcherInterface $eventDispatcher, GenerateToken $token)
    {
        $form = $this->createForm(SendMailType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $em = $this->getDoctrine()->getManager();
            /** @var User $user */
            $user = $em->getRepository(User::class)->findOneByEmail($email);

            if (!$user) {

                $this->addFlash(
                    'danger',
                    'cet email n\'existe pas'
                );
                return $this->redirectToRoute('register');


            }

            $user->setToken($token->generateToken());
            $em->persist($user);
            $em->flush();

            $event = new GenericEvent($user);
            $eventDispatcher->dispatch(Events::USER_RESETPASSWORD, $event);

            return $this->redirectToRoute('check_email');

        }



        return $this->render('security/change_password.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/change-password/{token}", name="change_password")
     * @Method={"POST"}
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param string $token
     * @return Response
     */
    public function changePasswordAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, $token)
    {


        $em = $this->getDoctrine()->getManager();
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneByToken($token);
        if (!$user) {
            return $this->redirectToRoute('login', ['register' => 'notfound']);
        }
        // if ($user->isTokenExpired()) {
        // return $this->redirectToRoute('security_connexion', ['register' => 'expired']);
        //}
        # checkif author is banned
        if ($user->isBanned()) {
            return $this->redirectToRoute('login', ['register' => 'banned']);
        }
        # checkif author is banned
        if ($user->isClosed()) {
            return $this->redirectToRoute('login', ['register' => 'closed']);
        }

        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $password = $form->get('password')->getData();
            $passwordencoded = $passwordEncoder->encodePassword($user, $password);
            $user->setPassword($passwordencoded);
            $user->setToken(null);
            $user->setActive();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'success',
                'votre mot de passe est bien réinitialisé vous pouvez vous connecter'
            );
            return $this->redirectToRoute('login');

        }


        return $this->render('security/reset_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/check-email", name="check_email")
     */
    public function checkEmail()
    {
        return $this->render('security/check_email.html.twig');
    }



}