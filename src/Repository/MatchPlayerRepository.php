<?php

namespace App\Repository;

use App\Entity\MatchPlayer;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MatchPlayer|null find($id, $lockMode = null, $lockVersion = null)
 * @method MatchPlayer|null findOneBy(array $criteria, array $orderBy = null)
 * @method MatchPlayer[]    findAll()
 * @method MatchPlayer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatchPlayerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MatchPlayer::class);
    }

//    /**
//     * @return MatchPlayer[] Returns an array of MatchPlayer objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MatchPlayer
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findNbParticipationByUser($iuserId)
    {
        return $this->createQueryBuilder('mp')
            ->select('COUNT(m)')
            ->where('mp.user = :user_id')->setParameter('user_id', $iuserId)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findLastMatch()
    {
        return $this->createQueryBuilder('mp')
            ->setMaxResults(1)
            ->orderBy('mp.id', 'DESC')
            ->getQuery()
            ->getSingleResult();
    }

    public function findMatchPlayersWhereIsNotPlayed(User $user)
    {
        return $this->createQueryBuilder('mp')
            ->join('mp.match', 'm')
            ->where('mp.user = :user')
            ->andWhere('mp.user = :user')
            ->setParameter('user', $user)
            ->andWhere('mp.isPlayed = 0')
            ->getQuery()
            ->getResult();
    }

    public function findScoresButForThisMP($user,MatchPlayer $matchPlayer)
    {
        return $this->createQueryBuilder('mp')
            ->where('mp.user = :user')
            ->setParameter(':user', $user)
            ->andWhere('mp.id != :matchPlayerId')
            ->setParameter(':matchPlayerId', $matchPlayer->getId())
            ->andWhere('mp.isPlayed = 1')
            ->getQuery()
            ->getResult();

    }
}
