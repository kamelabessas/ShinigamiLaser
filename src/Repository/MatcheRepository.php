<?php

namespace App\Repository;

use App\Entity\Matche;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Matche|null find($id, $lockMode = null, $lockVersion = null)
 * @method Matche|null findOneBy(array $criteria, array $orderBy = null)
 * @method Matche[]    findAll()
 * @method Matche[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatcheRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Matche::class);
    }

//    /**
//     * @return Match[] Returns an array of Match objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @param $array
     * @return Matche[] Returns an array of Matche objects witch their ids are in $array
     */
    public function findMatchesByIdsInBasket($array)
    {
        return $this->createQueryBuilder('m')
            ->where('m.id IN (:array)')
            ->setParameter('array', $array)
            ->getQuery()
            ->getResult();
    }

    /*public function findMatchesByUser($user)
    {
        return $this->createQueryBuilder('m')
            ->from('App:MatchPlayer', 'mp')
            ->leftJoin('mp', 'm')
            ->where('mp.user = :user')
            ->setParameter('user', $user)
            ->andWhere('mp.isPlayed = 0')
            ->getQuery()
            ->getResult();
    }*/

}
