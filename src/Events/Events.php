<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 29/03/2018
 * Time: 12:48
 */

namespace App\Events;



final class Events
{

    /**
     * For the event naming conventions, see:
     * https://symfony.com/doc/current/components/event_dispatcher.html#naming-conventions.
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     *
     * @var string
     */
    const USER_REGISTERED = 'user.registered';
    const USER_RESETPASSWORD ='user.resetpassword';




}