<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 29/03/2018
 * Time: 12:23
 */

namespace App\Events\EventSubscriber;

use App\Entity\User;
use App\Events\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Twig\Environment;


class RegistrationNotifySubscriber implements EventSubscriberInterface
{
    private $mailer;
    private $sender;
    private $twig;

    public function __construct(\Swift_Mailer $mailer, $sender, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->sender = $sender;
        $this->twig = $twig;
    }

    public static function getSubscribedEvents(): array
    {
        return [

            Events::USER_REGISTERED => 'onUserRegistrated'

        ];
    }

    /**
     * @param GenericEvent $event
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function onUserRegistrated(GenericEvent $event): void
    {
        /** @var User $user */

        $user= $event->getSubject();
        $message = (new \Swift_Message())
            ->setSubject('Email d\'activation de compte')
            ->setTo($user->getEmail())
            ->setFrom($this->sender)
            ->setBody($this->twig->render('security/activate_account.html.twig', [
                'user' => $user
            ]),'text/html');

        $this->mailer->send($message);
    }

}