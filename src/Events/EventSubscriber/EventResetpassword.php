<?php
/**
 * Created by PhpStorm.
 * User: haroun
 * Date: 02/04/2018
 * Time: 17:50
 */

namespace App\Events\EventSubscriber;


use App\Entity\User;
use App\Events\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Twig\Environment;


class EventResetpassword implements EventSubscriberInterface
{
    private $mailer;
    private $sender;
    private $twig;

    public function __construct(\Swift_Mailer $mailer, $sender, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->sender = $sender;
        $this->twig = $twig;
    }

    public static function getSubscribedEvents()
    {
        return [
            Events:: USER_RESETPASSWORD => 'onUserResetPassword'

        ];
    }


    /**
     * @param GenericEvent $event
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function onUserResetPassword(GenericEvent $event): void
    {
        /** @var User $user */

        $user = $event->getSubject();
        $message = (new \Swift_Message())
            ->setSubject('Email de réinitialisation de mot de passe')
            ->setTo($user->getEmail())
            ->setFrom($this->sender)
            ->setBody($this->twig->render('security/send_email.html.twig', [
                'user' => $user
            ]), 'text/html');

        $this->mailer->send($message);

    }


}