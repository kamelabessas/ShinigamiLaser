<?php

/**
 * namespace
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClubRepository")
 */
class Club
{
    /**
     * @ORM\Id
     * @Assert\Range(
     *     max="999",
     *     maxMessage = "You cannot be taller than {{ limit }}cm to enter"
     * )
     * @ORM\GeneratedValue()
     * @ORM\SequenceGenerator(sequenceName="id",allocationSize=100)
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min=3,
     *     minMessage = "Le nom d'etablissement doit avoir minimume 3 caractères"
     * )
     * @ORM\Column(type="string", length=50)
     */
    private $establishmentName;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $code;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=250)
     */
    private $address;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="club")
     */
    private $users;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Game", cascade={"persist"}, mappedBy="clubs")
     */
    private $games;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->games = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getEstablishmentName();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEstablishmentName()
    {
        return $this->establishmentName;
    }

    /**
     * @param mixed $establishmentName
     */
    public function setEstablishmentName($establishmentName)
    {
        $this->establishmentName = $establishmentName;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param $user
     */
    public function addUsers($user): void
    {
        $this->users[] = $user;
    }

    /**
     * @param $user
     */
    public function removeUsers($user): void
    {
        $this->users->removeElement($user);
    }


    /**
     * @return mixed
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * @param mixed $game
     */
    public function addGames($game): void
    {
        $this->games[] = $game;
    }

    /**
     * @param $game
     */
    public function removeGames($game): void
    {
        $this->games->removeElement($game);
    }

}
