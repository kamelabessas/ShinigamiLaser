<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardRepository")
 */
class Card
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $point;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreate;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbCardCreate;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    private $cardCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="card")
     */
    private $user;


    public function __construct()
    {

        $this->dateCreate = new \DateTime();
    }


    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param mixed $point
     */
    public function setPoint($point): void
    {
        $this->point = $point;
    }

    /**
     * @return mixed
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * @param mixed $dateCreate
     */
    public function setDateCreate($dateCreate): void
    {
        $this->dateCreate = $dateCreate;
    }

    /**
     * @return mixed
     */
    public function getCardCode()
    {
        return $this->cardCode;
    }

    /**
     * @param mixed $cardCode
     */
    public function setCardCode($cardCode): void
    {
        $this->cardCode = $cardCode;
    }

    /**
     * @return mixed
     */
    public function getNbCardCreate()
    {
        return $this->nbCardCreate;
    }

    /**
     * @param mixed $nbCardCreate
     */
    public function setNbCardCreate($nbCardCreate): void
    {
        $this->nbCardCreate = $nbCardCreate;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }


}
