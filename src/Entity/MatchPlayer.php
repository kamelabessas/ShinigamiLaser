<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatchPlayerRepository")
 */
class MatchPlayer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $score;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Matche", inversedBy="matchPlayers")
     */
    private $match;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="matchPlayers")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPlayed;

    public function __construct()
    {
        $this->createdDate = new \DateTime();
        $this->score = 0;
        $this->isPlayed = false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param mixed $score
     * @return MatchPlayer
     */
    public function setScore($score): MatchPlayer
    {
        $this->score = $score;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param mixed $createdDate
     * @return MatchPlayer
     */
    public function setCreatedDate($createdDate): MatchPlayer
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     * @return MatchPlayer
     */
    public function setEndDate($endDate): MatchPlayer
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * @param mixed $match
     * @return MatchPlayer
     */
    public function setMatch($match): MatchPlayer
    {
        $this->match = $match;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return MatchPlayer
     */
    public function setUser($user): MatchPlayer
    {
        $this->user = $user;
        
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsPlayed()
    {
        return $this->isPlayed;
    }

    /**
     * @param mixed $isPlayed
     * @return MatchPlayer
     */
    public function setIsPlayed($isPlayed): MatchPlayer
    {
        $this->isPlayed = $isPlayed;

        return $this;
    }

}
