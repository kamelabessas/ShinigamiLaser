<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatcheRepository")
 */
class Matche
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $duration;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 1,
     *      max = 20,
     *      minMessage = "You must be at least {{ limit }} player to enter",
     *      maxMessage = "You cannot be taller than {{ limit }} player to enter"
     * )
     * @Assert\NotBlank()
     */
    private $minNbPlayer;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Expression("value >= this.getMinNbPlayer()")
     * @Assert\NotBlank()
     */
    private $maxNbPlayer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MatchPlayer", mappedBy="match")
     */
    private $matchPlayers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Game", inversedBy="matches")
     */
    private $game;

    public function __construct()
    {
        $this->matchPlayers = new ArrayCollection();
        $this->date = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Matche
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getMinNbPlayer(): ?int
    {
        return $this->minNbPlayer;
    }

    public function setMinNbPlayer(int $minNbPlayer): self
    {
        $this->minNbPlayer = $minNbPlayer;

        return $this;
    }

    public function getMaxNbPlayer(): ?int
    {
        return $this->maxNbPlayer;
    }

    public function setMaxNbPlayer(int $maxNbPlayer): self
    {
        $this->maxNbPlayer = $maxNbPlayer;

        return $this;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getMatchPlayer()
    {
        return $this->matchPlayers;
    }

    /**
     * @param MatchPlayer $matchPlayer
     */
    public function addMatchPlayer($matchPlayer)
    {
        $this->matchPlayers[] = $matchPlayer;
    }

    /**
     * @param MatchPlayer $matchPlayer
     * @return $this
     */
    public function removeMatchPlayer($matchPlayer)
    {
        $this->matchPlayers->removeElement($matchPlayer);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * @param $game
     */
    public function setGame($game): void
    {
        $this->game = $game;
    }

}
