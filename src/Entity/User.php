<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(fields="email", message="Email déjà pris")
 * @UniqueEntity(fields="username", message="username déjà pris")
 *
 */
class User implements AdvancedUserInterface

{
    /**
     * Constant for inactive Author, register but didn't validate email confirmation
     */
    const INACTIVE = 0;
    /**
     * Constant for registerd Author
     */
    const ACTIVE = 1;
    /**
     * Constant for Author closed account
     */
    const CLOSED = 2;
    /**
     * Constant for Author banned account
     */
    const BANNED = 3;
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=20)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=20)
     */
    private $username;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     *
     */
    private $email;

    /**
     * @ORM\Column(type="datetime")
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     *
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Club", inversedBy="users")
     */
    private $club;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Card", mappedBy="user")
     */
    private $card;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $userCode;

    /**
     * @var int
     * @ORM\Column(type="string", nullable=true, unique=true, length=64)
     */
    private $token;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $roles = [];

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, nullable=true)
     *
     */
    private $service;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MatchPlayer", mappedBy="user")
     */
    private $matchPlayers;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, nullable=true)
     *
     */
    private $picture;

    /**
     * @var string
     *
     * @ORM\Column(type="string",  length=255, nullable=true)
     *
     */
    private $pictureName;

    /**
     * User account state ( 0 = inactive ; 1 = active ; 2 = closed ; 3 = banned  )
     * @ORM\Column(type="integer")
     * @see User contants
     */
    private $status;


    public function __construct()
    {

        $this->matchPlayers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param mixed $birthdate
     */
    public function setBirthdate($birthdate): void
    {
        $this->birthdate = $birthdate;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @param mixed $club
     */
    public function setClub($club): void
    {
        $this->club = $club;
    }

    /**
     * @return mixed
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param mixed $card
     */
    public function setCard($card): void
    {
        $this->card = $card;
    }

    /**
     * @return string
     */
    public function getUserCode(): ?string
    {
        return $this->userCode;
    }

    /**
     * @param string $userCode
     */
    public function setUserCode(string $userCode): void
    {
        $this->userCode = $userCode;
    }

    /**
     * @return string
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     * @return User
     */
    public function setRoles($roles): User
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param string $service
     */
    public function setService(string $service): void
    {
        $this->service = $service;
    }


    /**
     * @return mixed
     */
    public function getMatchPlayer()
    {
        return $this->matchPlayers;
    }

    /**
     * @param $matchPlayer
     */
    public function addMatchPlayer($matchPlayer): void
    {
        $this->matchPlayers[] = $matchPlayer;
    }

    /**
     * @param $matchPlayer
     */
    public function removeMatchPlayer($matchPlayer): void
    {
        $this->matchPlayers->remove($matchPlayer);
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture($picture): void
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getPictureName()
    {
        return $this->pictureName;
    }

    /**
     * @param mixed $pictureName
     */
    public function setPictureName($pictureName): void
    {
        $this->pictureName = $pictureName;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;

    }

    /**
     * @return User
     */
    public function setInactive(): User
    {
        $this->status = $this::INACTIVE;
        return $this;
    }

    /**
     * @return User
     */
    public function setActive(): User
    {
        $this->status = $this::ACTIVE;
        return $this;
    }

    /**
     * @return User
     */
    public function setClosed(): User
    {
        $this->status = $this::CLOSED;
        return $this;
    }

    /**
     * @return User
     */
    public function setBanned(): User
    {
        $this->status = $this::BANNED;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBanned(): bool
    {
        return $this->status == $this::BANNED;
    }

    /**
     * @return bool
     */
    public function isClosed(): bool
    {
        return $this->status == $this::CLOSED;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled(): bool
    {
        return $this->status == $this::ACTIVE;
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired(): bool
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked(): bool
    {
        return true;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired(): bool
    {
        return true;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        return true;
    }
}
