<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Matche;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MatcheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'match.name',
                'required' => true,
                'translation_domain' => 'match',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'match.name'
                ]])
            ->add('type', TextType::class, [
                'label' => 'match.type',
                'required' => true,
                'translation_domain' => 'match',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'match.type'
                ]])
            ->add('duration', IntegerType::class, [
                'label' => 'match.duration (min)',
                'required' => true,
                'translation_domain' => 'match',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'match.duration'
                ]])
            ->add('minNbPlayer', IntegerType::class, [
                'label' => 'match.minNbPlayer',
                'required' => true,
                'translation_domain' => 'match',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'match.minNbPlayer'
                ]])
            ->add('maxNbPlayer', IntegerType::class, [
                'label' => 'match.maxNbPlayer',
                'required' => true,
                'translation_domain' => 'match',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'match.maxNbPlayer'
                ]])
            ->add('game', EntityType::class, [
                'class' => Game::class,
                'label' => 'match.game',
                'required' => true,
                'translation_domain' => 'match',
                'attr' => [
                    'class' => 'form-control'
                ]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Matche::class,
        ]);
    }
}
