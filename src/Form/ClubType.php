<?php

namespace App\Form;

use App\Entity\Club;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClubType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('establishmentName', TextType::class, [
                'label' => 'club.name',
                'translation_domain' => 'club',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'club.name'
                ]])
            ->add('address', TextType::class, [
                'label' => 'club.address',
                'translation_domain' => 'club',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'club.address'
                ]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Club::class,
        ]);
    }
}
