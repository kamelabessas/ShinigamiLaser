<?php
/**
 * Created by PhpStorm.
 * User: haroun
 * Date: 10/04/2018
 * Time: 21:01
 */

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsletterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            # Champ Email
            ->add('email', EmailType::class, [
                'required'  => true,
                'label'     => false,
                'attr'      => [
                    'placeholder'   => 'Saisissez votre Email',
                    'class'         => 'form-control'
                ]
            ])

            # Bouton Submit
            ->add('submit', SubmitType::class, [
                'label'  => 'Je m\'inscris !',
                'attr'      => [
                    'class'   => 'btn btn-primary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Newsletter::class
        ]);
    }

}