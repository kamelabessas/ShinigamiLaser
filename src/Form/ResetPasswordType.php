<?php
/**
 * Created by PhpStorm.
 * User: haroun
 * Date: 02/04/2018
 * Time: 18:20
 */

namespace App\Form;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResetPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'translation_domain' => 'security',
                'first_options'  => array(
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'security.password'
                ]),
                'second_options' => array(
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'security.password'
                ]),
            ))
            ->add('submit', SubmitType::class, [
                'label' => 'Réinitialiser',
                'attr' => [
                    'class' => 'button  button-block'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }


}