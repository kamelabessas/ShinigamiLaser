<?php
/**
 * Created by PhpStorm.
 * User: haroun
 * Date: 15/04/2018
 * Time: 14:38
 */

namespace App\Form;


use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditUserByAdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('service', ChoiceType::class, [
                'label' => false,
                'choices' => array(
                    'Marketing' => 'Marketing',
                    'Quality_Manager' => 'Quality_manager',
                    'Admin' => 'Admin',
                    'Super_Admin' => 'Super_Admin',

                ),
                'attr' => [
                    'class' => 'input',

                ],
            ])

            ->add('submit', SubmitType::class, [
                'label' => 'Attribuer un role',
                'attr' => [
                    'class' => 'button  button-block'
                ]
            ]);


    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }




}