<?php
/**
 * Created by PhpStorm.
 * User: haroun
 * Date: 28/03/2018
 * Time: 15:16
 */

namespace App\Form;



use App\Entity\Club;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => false,
                'required' => true,
                'translation_domain' => 'security',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'security.name'
                ]])
            ->add('username', TextType::class, [
                'label' => false,
                'required' => true,
                'translation_domain' => 'security',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'security.nickname'
                ]])
            ->add('email', EmailType::class, [
                'label' => false,
                'required' => true,
                'translation_domain' => 'security',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'security.email'
                ]])
            ->add('address', TextType::class, [
                'label' => false,
                'required' => true,
                'translation_domain' => 'security',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'security.address'
                ]])
            ->add('club', EntityType::class, [
                'class' => Club::class,
                'label' => false,
                'required' => true,
                'choice_label' => 'establishmentName',
                'multiple'      => false,
                'expanded'      => false,
                'translation_domain' => 'security.club',
                'attr' => [
                    'class' => 'form-control'
                ]])

            ->add('birthdate', BirthdayType::class, [
                'translation_domain' => 'security',
                'label' => 'security.birthdate'])
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'translation_domain' => 'security',
                'first_options' => array(
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'security.password'
                    ]),
                'second_options' => array(
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'security.password2'
                    ]),
                'required' => true,

            ))
            ->add('submit', SubmitType::class, [
                'label' => 'Inscription',
                'attr' => [
                    'class' => 'button  button-block'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
