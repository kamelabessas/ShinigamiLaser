<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 12/04/2018
 * Time: 13:27
 */

namespace App\Form;


use App\Entity\Club;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditUserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name', TextType::class, [
                'label' => false,
                'required' => true,
                'translation_domain' => 'security',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'security.name'
                ]])
            ->add('username', TextType::class, [
                'label' => false,
                'required' => true,
                'translation_domain' => 'security',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'security.picture'
                ]])
            ->add('email', TextType::class, [
                'label' => false,
                'required' => true,
                'translation_domain' => 'security',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'security.picture'
                ]])
            ->add('picture', FileType::class, [
                'data_class' => null,
                'label' => false,
                'required' => false,
                'translation_domain' => 'game',
                'attr' => [
                    'class' => 'form-control dropify',
                    'placeholder' => 'game.img'
                ]])
            ->add('pictureName', TextType::class, [
                'label' => false,
                'required'      => true,
                'attr'  => [
                    'class'         => 'form-control',
                    'placeholder'   => 'Titre de l\'image'
                ]
            ])
            ->add('address', TextType::class, [
                'label' => false,
                'required' => true,
                'translation_domain' => 'security',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'security.address'
                ]])
            ->add('club', EntityType::class, [
                'class' => Club::class,
                'label' => false,
                'required' => true,
                'choice_label' => 'establishmentName',
                'multiple'      => false,
                'expanded'      => false,
                'translation_domain' => 'security.club',
                'attr' => [
                    'class' => 'form-control'
                ]])


            ->add('submit', SubmitType::class, [
                'label' => 'Editer mon profil',
                'attr' => [
                    'class' => 'button  button-block'
                ]
            ]);


    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }


}