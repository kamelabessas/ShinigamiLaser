<?php

namespace App\Form;

use App\Entity\Club;
use App\Entity\Game;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'game.name',
                'required' => true,
                'translation_domain' => 'game',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'game.name'
                ]])
            ->add('category', TextType::class, [
                'label' => 'game.category',
                'required' => true,
                'translation_domain' => 'game',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'game.category'
                ]])
            ->add('description', TextareaType::class, [
                'label' => 'game.description',
                'required' => false,
                'translation_domain' => 'game',
                'attr' => [
                    'class' => 'form-control'
                ]])
            ->add('price', MoneyType::class, [
                'label' => 'game.price',
                'required' => true,
                'translation_domain' => 'game',
                'attr' => [
                    'class' => 'form-control',
                    'id' => "addDollarSignHere",
                    'ext' => "100.00",
                    'placeholder' => 'game.price'
                ]])
            ->add('img', FileType::class, [
                'data_class' => null,
                'label' => 'game.img',
                'required' => false,
                'translation_domain' => 'game',
                'attr' => [
                    'class' => 'form-control dropify',
                    'placeholder' => 'game.img'
                ]])
            ->add('clubs', EntityType::class, [
                'class' => Club::class,
                'label' => 'game.establishment',
                'required' => true,
                'translation_domain' => 'game',
                'multiple' => true,
                'attr' => [
                    'class' => 'form-control'
                ]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Game::class,
        ]);
    }
}
