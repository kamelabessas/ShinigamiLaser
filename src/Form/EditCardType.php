<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 20/04/2018
 * Time: 14:54
 */

namespace App\Form;


use App\Entity\Card;
use App\Entity\Club;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditCardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('cardCode',TextType::class, [
                'label' => 'Code carte',
                'translation_domain' => 'card',

            ])

            ->add('dateCreate', DateType::class, [
                'label' => 'Date création',
                'translation_domain' => 'card',

            ])
            ->add('nbCardCreate', IntegerType::class, [
                'label' => 'Nombre de création',
                'translation_domain' => 'card',

            ])

            ->add('point', IntegerType::class, [
                'label' => 'nombre de point',
                'translation_domain' => 'card',

            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Editer',
                'attr' => [
                    'class' => 'button  button-block'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Card::class,
        ));
    }

}