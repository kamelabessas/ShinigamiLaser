<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 16/04/2018
 * Time: 16:59
 */

namespace App\DataFixtures;


use App\Entity\Game;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class GameFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $this->loadGames($manager);

    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    private function loadGames(ObjectManager $manager)
    {
        foreach ($this->getGameData() as $key =>[$name, $category, $description, $price, $img, $slug]) {
            $game = new Game();
            $game->setName($name);
            $game->setCategory($category);
            $game->setDescription($description);
            $game->setPrice($price);
            $game->setImg($img);
            $game->setSlug($slug);
            $game->addClubs(array($this->getReference('club1')));
            $manager->persist($game);
            $this->addReference('game'.$key, $game);
        }
        $manager->flush();
        }


    private function getGameData(): array
    {
        return [
            // $gameData = [$name, $category, $description, $price, $img, $slug];
            ['jeux1', 'categorie1', 'description1', 15, 'image1', 'slug1'],
            ['jeux2', 'categorie2','description2', 20, 'image2', 'slug2' ],
            ['jeux3', 'categorie2','description3', 25, 'image3', 'slug3']
        ];
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }

}