<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 16/04/2018
 * Time: 16:53
 */

namespace App\DataFixtures;


use App\Entity\Club;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\BadMethodCallException;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class ClubFixtures extends Fixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        $this->loadClubs($manager);

    }

    /**
     * @param ObjectManager $manager
     * @throws BadMethodCallException
     */
    private function loadClubs(ObjectManager $manager)
    {

        foreach ($this->getClubData() as $key => [$establishmentName, $address, $code]) {
            $club = new Club();
            $club->setEstablishmentName($establishmentName);
            $club->setAddress($address);
            $club->setCode($code);
            $manager->persist($club);
            $this->addReference('club'.$key, $club);


        }
        $manager->flush();
    }

    private function getClubData(): array
    {
        return [
            // $clubData = [$establishmentName, $address, $code];
            ['establishmentName1', '60 rue de belleville', 342],
            ['establishmentName2', '65 rue de jaures', 543],
            ['establishmentName3', '80 rue de stalingrad', 987]
        ];
    }


    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}