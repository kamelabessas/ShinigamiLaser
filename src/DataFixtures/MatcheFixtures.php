<?php
/**
 * Created by PhpStorm.
 * User: haroun
 * Date: 16/04/2018
 * Time: 20:54
 */

namespace App\DataFixtures;


use App\Entity\Matche;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MatcheFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $this->loadMatchs($manager);

    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    private function loadMatchs(ObjectManager $manager)
    {
        foreach ($this->getMatcheData() as $key => [$name, $type, $date, $duration, $minNbPlayer, $maxNbPlayer]) {
            $matche = new Matche();
            $matche->setName($name);
            $matche->setType($type);
            $matche->setDate($date);
            $matche->setDuration($duration);
            $matche->setMinNbPlayer($minNbPlayer);
            $matche->setMaxNbPlayer($maxNbPlayer);
            $matche->setGame($this->getReference('game1'));
            $manager->persist($matche);
            $this->addReference('matche'.$key, $matche);
        }
        $manager->flush();

    }

    private function getMatcheData(): array
    {
        return [
            // $matcheData = [$name, $type, $date, $duration, $minNbPlayer, $maxNbPlayer, $matchPlayers, $game];
            ['matche1', 'type1', new \DateTime('2008-10-12'), '15', 2, 5],
            ['matche2', 'type1', new \DateTime('2008-10-12'), '15', 1, 4],
            ['matche3', 'type1', new \DateTime('2008-10-12'), '15', 1, 3]
        ];
    }


    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 4;
    }


}