<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 16/04/2018
 * Time: 16:56
 */

namespace App\DataFixtures;


use App\Entity\Card;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class CardFixtures extends Fixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    public function load(ObjectManager $manager)
    {
        $this->loadCards($manager);
    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    private function loadCards(ObjectManager $manager)
    {
        foreach ($this->getCardData() as $key => [$point, $dateCreate, $cardCode, $nbCreate]) {
            $card = new Card();
            $card->setPoint($point);
            $card->setDateCreate($dateCreate);
            $card->setCardCode($cardCode);
            $card->setNbCardCreate($nbCreate);
            $card->setUser($this->getReference('user1'));
            $manager->persist($card);
            $this->addReference('card'.$key, $card);
        }
        $manager->flush();


    }

    private function getCardData(): array
    {
        return [
            // GamePlayerData =[$point, $dateCreation,$cardCode, $nbCreate  $user]
            [15, new \DateTime('2009-11-03'), '1280945935', 2],
            [10, new \DateTime('2010-08-28'), '6053671258', 1],
            [15, new \DateTime('2013-10-16'), '1096521478', 3]
        ];
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}