<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 18/04/2018
 * Time: 15:14
 */

namespace App\DataFixtures;


use App\Entity\MatchPlayer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\BadMethodCallException;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MatchPlayerFixtures extends Fixture implements  OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws BadMethodCallException
     */
    public function load(ObjectManager $manager)
    {
        $this->loadMatchPlayers($manager);
    }

    /**
     * @param ObjectManager $manager
     *  @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */
    private function loadMatchPlayers(ObjectManager $manager)
    {
        foreach ($this->getMatchPlayerData() as $key => [$score, $dateCreate, $endDate, $isPlayed]) {
            $matchPlayer = new MatchPlayer();
            $matchPlayer->setScore($score);
            $matchPlayer->setCreatedDate($dateCreate);
            $matchPlayer->setEndDate($endDate);
            $matchPlayer->setIsPlayed(true);
            $matchPlayer->setMatch($this->getReference('matche1'));
            $matchPlayer->setUser($this->getReference('user1'));
            $manager->persist($matchPlayer);

        }
        $manager->flush();


    }

    private function getMatchPlayerData(): array
    {
        return [

            [15, new \DateTime('2009-11-03'), new \DateTime('2010-11-03'), true],
            [10, new \DateTime('2010-08-28'), new \DateTime('2009-12-03'), false],
            [15, new \DateTime('2013-10-16'), new \DateTime('2009-11-15'), true]
        ];
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }

}