<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 09/04/2018
 * Time: 11:25
 */

namespace App\EventListener;


use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Route;

class BasketValidationListener
{
    private $container;
    private $session;

    public function __construct()
    {
        $this->container = 'test';
        //$this->session = $session;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        return true;
    }
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest'
        ];
    }
}