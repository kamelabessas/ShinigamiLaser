<?php

namespace App\Service\Twig;


use Twig\Extension\AbstractExtension;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [

            new \Twig_Filter('tva', [$this, 'calculTva']),

        ];
    }

    public function calculTva($priceHT)
    {
        return round($priceHT / 19, 2);
    }


}
