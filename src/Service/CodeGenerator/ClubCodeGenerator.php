<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 13/04/2018
 * Time: 15:45
 */

namespace App\Service\CodeGenerator;


class ClubCodeGenerator
{
    public function establishmentCodeGenerator()
    {
        return mt_rand(100, 999);
    }

}