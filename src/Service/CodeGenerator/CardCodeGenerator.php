<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 13/04/2018
 * Time: 15:46
 */

namespace App\Service\CodeGenerator;


class CardCodeGenerator
{
    public function cardCodeGenerator($establishmentCode, $userCode)
    {
        //$establishmentCodeInString = str_pad($establishmentCode, 3, '0', STR_PAD_LEFT);
        //$userCodeInString = str_pad($userCode, 6, '0', STR_PAD_LEFT);

        //$allCode = $establishmentCodeInString . $userCodeInString;
       if (strlen($establishmentCode) != 3 || strlen($userCode) != 6 )
           return false;
        $allCode = $establishmentCode . $userCode;

        $checkSum = $this->checkSumGenerator($allCode);

        $cardCode = $establishmentCode.$userCode.$checkSum;

        return (string)$cardCode;
    }

    public function checkSumGenerator($allCode)
    {
        //$integerCode = intval($allCode);
        $sum = 0;
        if (strlen($allCode) != 9)
            return false;
        while (strlen(strval($allCode)) != 1) {
            $sum += $allCode % 10;
            $allCode /= 10;
        }

        $modulo = $sum % 9;
        return $modulo;
    }

}