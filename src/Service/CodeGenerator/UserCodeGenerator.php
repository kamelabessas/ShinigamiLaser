<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 13/04/2018
 * Time: 15:46
 */

namespace App\Service\CodeGenerator;


class UserCodeGenerator
{
    public function userCodeGenerator()
    {
        return mt_rand(100000, 999999);
    }

}