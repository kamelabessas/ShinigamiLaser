<?php

namespace App\Service\Match;


use App\Entity\MatchPlayer;
use Doctrine\Common\Persistence\ObjectManager;

class ScoreGenerator
{
    /** @var ObjectManager $em */
    private $em;

    /**
     * ScoreGenerator constructor.
     * @param ObjectManager $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    public function generateScoreToPlayer(MatchPlayer $matchPlayer)
    {
        $match = $matchPlayer->getMatch();
        $duration = $match->getDuration();
        $date = new \DateTime();
        $endDate = $date->add(new \DateInterval('PT'.$duration.'M'));
        $matchPlayer->setEndDate($endDate);
        $score = $this->scoreGenerator();
        $matchPlayer->setScore($score);
        $matchPlayer->setIsPlayed(true);
        $this->em->persist($matchPlayer);
        $this->em->flush();

        return true;
    }

    private function scoreGenerator()
    {
        return mt_rand(0,20);
    }
}