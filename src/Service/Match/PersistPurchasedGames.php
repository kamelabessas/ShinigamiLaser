<?php
/**
 * Created by PhpStorm.
 * User: Mitra
 * Date: 15/04/2018
 * Time: 10:30
 */

namespace App\Service\Match;


use App\Entity\Matche;
use App\Entity\MatchPlayer;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class PersistPurchasedGames
{
    /** @var ObjectManager $em */
    private $em;

    /**
     * ScoreGenerator constructor.
     * @param ObjectManager $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    public function insertPurchasedGamesInDb(array $matchIds,User $user)
    {
        foreach ($matchIds as $matchId) {
            $matchPlayer = new MatchPlayer();
            $match = $this->em->getRepository(Matche::class)->find($matchId);
            $matchPlayer->setMatch($match);
            $matchPlayer->setUser($user);
            $this->em->persist($matchPlayer);
        }
        $this->em->flush();
        return true;
    }

}