<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 09/04/2018
 * Time: 16:02
 */

namespace App\Service\Token;


class GenerateToken
{
    public function generateToken()
    {
        $token = md5(uniqid(rand(), true));

        return $token;
    }
}