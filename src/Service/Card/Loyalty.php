<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 13/04/2018
 * Time: 11:01
 */

namespace App\Service\Card;


use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class Loyalty
{
    private $em;

    /**
     * Loyalty constructor.
     * @param ObjectManager $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;

    }

    public function pointsLoyalty(User $user)
    {
        $card = $user->getCard();
        $points = $card->getPoints();
        $repository = $this->em->getRepository('App:MatchPlayer');
        $nbParticipations = $repository->findNbParticipationByUser($user->getId());
        if ($nbParticipations > 3) {
            $points = $points + 5;
            $this->em->persist($card);
            $this->em->flush();
        }



    }


}