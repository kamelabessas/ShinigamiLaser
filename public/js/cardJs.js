
window.onload = function (e) {
    function onFocus(element) {
        console.log("onFocus");
        var targetElement = event.target || event.srcElement
        targetElement.parentElement.getElementsByClassName("mdi")[0].classList.add("active");
    }
    function onBlur(element){
        var targetElement = event.target || event.srcElement
        targetElement.parentElement.getElementsByClassName("mdi")[0].classList.remove("active");
    }
    var materialInputElements = document.getElementsByClassName('material-input iconic');
    Array.from(materialInputElements).forEach(function (element) {
        element.addEventListener('focus', function(e){
            onFocus(e);
        },true);
        element.addEventListener('blur', function (e) {
            onBlur(e);
        }, true);
    });
};