<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 16/04/2018
 * Time: 15:52
 */

namespace App\Tests\Common\Traits\String;


use App\Common\Traits\String\SlugifyTrait;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;


class SlugifyTraitTest extends TestCase
{

    public function testSluggifyReturnsSluggifiedString()
    {
        $originalString = 'This string will be sluggified';
        $expectedResult = 'this-string-will-be-sluggified';

        $result = SlugifyTrait::slugify($originalString);

        $this->assertEquals($expectedResult, $result);
    }

    public function testSluggifyReturnsExpectedForStringsContainingNumbers()
    {
        $originalString = 'This1 string2 will3 be 44 sluggified10';
        $expectedResult = 'this1-string2-will3-be-44-sluggified10';

        $result = SlugifyTrait::slugify($originalString);
        $this->assertEquals($expectedResult, $result);
    }

    public function testSluggifyReturnsExpectedForEmptyStrings()

    {
        $originalString = '';
        $expectedResult = 'n-a';
        $result = SlugifyTrait::slugify($originalString);
        $this->assertEquals($expectedResult, $result);
    }


}