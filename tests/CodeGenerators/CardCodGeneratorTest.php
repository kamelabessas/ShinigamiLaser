<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 16/04/2018
 * Time: 15:08
 */

namespace App\Tests\CodeGenerators;


use App\Service\CodeGenerator\CardCodeGenerator;
use PHPUnit\Framework\TestCase;

class CardCodGeneratorTest extends TestCase
{
    public function testCheckSumGenerator9Number()
    {
        $cardCodeGenerator = new CardCodeGenerator();
        $res = $cardCodeGenerator->checkSumGenerator(265987156);

        $this->assertEquals(4, $res);

    }

    public function testCheckSumGenerator3Number()
    {
        $cardCodeGenerator = new CardCodeGenerator();
        $res = $cardCodeGenerator->checkSumGenerator(415);

        $this->assertEquals(false, $res);

    }

    public function testCardCodeGenerator()
    {
        $cardCodeGenerator = new CardCodeGenerator();
        $code = $cardCodeGenerator->cardCodeGenerator(456, 985469);

        $this->assertEquals(4569854692, $code);
    }

    public function testCardCodeGeneratorClubCode5Number()
    {
        $cardCodeGenerator = new CardCodeGenerator();
        $code = $cardCodeGenerator->cardCodeGenerator(11612, 985469);

        $this->assertEquals(false, $code);
    }

    public function testCardCodeGeneratorUserCode4Number()
    {
        $cardCodeGenerator = new CardCodeGenerator();
        $code = $cardCodeGenerator->cardCodeGenerator(116, 5469);

        $this->assertEquals(false, $code);
    }

}