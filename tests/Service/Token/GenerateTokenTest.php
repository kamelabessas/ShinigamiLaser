<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 16/04/2018
 * Time: 11:14
 */

namespace App\Tests\Service\Token;


use App\Service\Token\GenerateToken;
use PHPUnit\Framework\TestCase;

class GenerateTokenTest extends TestCase
{
    public function TokenIsString()
    {

        $generateToken = new GenerateToken();
        $this->assertInternalType('string', $generateToken->generateToken());

    }


    public function testGenerateToken_tokenIsNotEmpty()
    {
        $token = new GenerateToken();
        $token1 = $token->generateToken();
        $this->assertNotEmpty($token1);
    }

    public function testGenerateToken_isUnique()
    {
        $token = new GenerateToken();
        $token1 = $token->generateToken();
        $token2 = $token->generateToken();
        $token3 = $token->generateToken();

        $this->assertNotEquals($token1, $token2);
        $this->assertNotEquals($token2, $token3);
        $this->assertNotEquals($token1, $token3);
    }

    public function testGenerateToken_isOnlyPreciseToByte()
    {
        $token = new GenerateToken();
        $token1 = $token->generateToken();
        $this->assertEquals(strlen ($token1), 32);


    }


}