<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 16/04/2018
 * Time: 17:11
 */

namespace App\Tests\Match;


use App\Entity\Matche;
use App\Entity\MatchPlayer;
use App\Entity\User;
use App\Service\Match\PersistPurchasedGames;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;

class PersistPurchasedGamesTest extends TestCase
{
    public function testInsertPurchasedGamesInDb()
    {
        $user = new User();
        $user->setName('myName');
        $user->setEmail('email@example.com');

        $match = new Matche();
        $match->setName('match No 1');
        $match->setSlug('match-No-1');
        $matchRepository = $this->createMock(ObjectRepository::class);

        $matchRepository->expects($this->any())
            ->method('find')
            ->willReturn($match);

        $objectManager = $this->createMock(ObjectManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($matchRepository);


        $objectManager->expects($this->exactly(3))
            ->method('persist')
            ->willReturn(true);
        $objectManager->expects($this->once())
            ->method('flush')
            ->willReturn(true);

        $persistPurchasedGames = new PersistPurchasedGames($objectManager);

        $this->assertTrue($persistPurchasedGames->insertPurchasedGamesInDb([1,2,3], $user));
    }

}