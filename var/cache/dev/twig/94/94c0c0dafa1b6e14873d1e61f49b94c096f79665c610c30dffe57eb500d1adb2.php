<?php

/* components/_footer.html.twig */
class __TwigTemplate_12580cd9e8bb17dd137b43c066b22439570fffd24ae7dcb3be1ac4943bb1e77d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "components/_footer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "components/_footer.html.twig"));

        // line 2
        echo "
<!--footer-->
<footer class=\"footer\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-4  col-sm-4 col-xs-12\">
                <div class=\"about\">
                    <a href=\"";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("index");
        echo "\" class=\"logo\">
                        <img alt=\"\" src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" width=\"50\" />
                    </a>
                </div>
            </div>
        </div>
        <!--All right-->
        <div class=\"allright\">
            <div class=\"row\">
                <div class=\"col-sm-6 col-xs-12\">
                    <p> © 2018 <a href=\"#\">Shinigami Laser</a>. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "components/_footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 10,  38 => 9,  29 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'footer' %}

<!--footer-->
<footer class=\"footer\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-4  col-sm-4 col-xs-12\">
                <div class=\"about\">
                    <a href=\"{{ url('index') }}\" class=\"logo\">
                        <img alt=\"\" src=\"{{ asset('images/logo.png') }}\" width=\"50\" />
                    </a>
                </div>
            </div>
        </div>
        <!--All right-->
        <div class=\"allright\">
            <div class=\"row\">
                <div class=\"col-sm-6 col-xs-12\">
                    <p> © 2018 <a href=\"#\">Shinigami Laser</a>. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
", "components/_footer.html.twig", "/Applications/MAMP/htdocs/shinigami_laser/templates/components/_footer.html.twig");
    }
}
