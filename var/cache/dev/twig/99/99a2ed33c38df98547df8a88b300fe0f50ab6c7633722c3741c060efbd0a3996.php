<?php

/* card/show_card.html.twig */
class __TwigTemplate_bf9df8401ea7f42d2d3ae4995c6aa4d1418aa8bd4aebbe36c8c62992df2da662 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "card/show_card.html.twig", 1);
        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "card/show_card.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "card/show_card.html.twig"));

        // line 5
        $context["active"] = "card";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 7
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("card.show.card", array(), "card"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/styleCard.css"), "html", null, true);
        echo ">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 16
        echo "    <div class=\"main-content\">
        <div class=\"card-checkout\">
            <div class=\"left-content\">
                <ul class=\"list\">
                    <li>
                        <h5>Code</h5>
                        <h4>";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["card"]) || array_key_exists("card", $context) ? $context["card"] : (function () { throw new Twig_Error_Runtime('Variable "card" does not exist.', 22, $this->source); })()), "cardCode", array()), "html", null, true);
        echo "</h4>
                    </li>
                    <li>
                        <h5>Date création</h5> <h4>";
        // line 25
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["card"]) || array_key_exists("card", $context) ? $context["card"] : (function () { throw new Twig_Error_Runtime('Variable "card" does not exist.', 25, $this->source); })()), "dateCreate", array()), "d/m/Y"), "html", null, true);
        echo "</h4>
                    </li>
                    <li>
                        <h5></h5>
                        <h4><i class=\"mdi mdi-currency-try\"></i></h4>
                    </li>
                </ul>
                <button class=\"back-button\"> <i class=\"mdi mdi-chevron-left\"></i> Shinigami Laser</button>
            </div>
            <div class=\"right-content\">
                <div class=\"top-content\">
                    <h5>Carte de fidélité</h5><i class=\"mdi mdi-chevron-right\"></i>
                    <img class=\"card-logo\" src=\"https://image.flaticon.com/icons/svg/179/179449.svg\" />
                </div>
                <div class=\"middle-content\">
                    <div class=\"material-input-group\">
                        <i class=\"mdi mdi-account\"></i>
                        <input type=\"text\" class=\"material-input iconic\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["card"]) || array_key_exists("card", $context) ? $context["card"] : (function () { throw new Twig_Error_Runtime('Variable "card" does not exist.', 42, $this->source); })()), "user", array()), "name", array()), "html", null, true);
        echo "\" required>
                        <span class=\"highlight\"></span>
                        <span class=\"material-input-bar\"></span>
                        <label class=\"material-input-label iconic\">Nom</label>
                    </div>
                    <div class=\"material-input-group\">
                        <i class=\"mdi mdi-credit-card\"></i>
                        <input type=\"text\" class=\"material-input iconic\" value=\"";
        // line 49
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["card"]) || array_key_exists("card", $context) ? $context["card"] : (function () { throw new Twig_Error_Runtime('Variable "card" does not exist.', 49, $this->source); })()), "user", array()), "username", array()), "html", null, true);
        echo "\" required>
                        <span class=\"highlight\"></span>
                        <span class=\"material-input-bar\"></span>
                        <label class=\"material-input-label iconic\">Prenom</label>
                    </div>
                    <div class=\"material-input-group date-input\">
                        <i class=\"mdi mdi-calendar-blank\"></i>
                        <input type=\"text\" class=\"material-input iconic\" value=\"";
        // line 56
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["card"]) || array_key_exists("card", $context) ? $context["card"] : (function () { throw new Twig_Error_Runtime('Variable "card" does not exist.', 56, $this->source); })()), "user", array()), "birthdate", array()), "d/m/Y"), "html", null, true);
        echo "\" required>
                        <span class=\"highlight\"></span>
                        <span class=\"material-input-bar\"></span>
                        <label class=\"material-input-label iconic\">Date de naissance </label>
                    </div>
                    <div class=\"material-input-group cvv-input\">
                        <i class=\"mdi mdi-numeric\"></i>
                        <input type=\"text\" class=\"material-input iconic\" value=\"";
        // line 63
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["card"]) || array_key_exists("card", $context) ? $context["card"] : (function () { throw new Twig_Error_Runtime('Variable "card" does not exist.', 63, $this->source); })()), "point", array()), "html", null, true);
        echo "\" required>
                        <span class=\"highlight\"></span>
                        <span class=\"material-input-bar\"></span>
                        <label class=\"material-input-label iconic\">Points</label>
                    </div>
                </div>
                <div class=\"bottom-content\">
                    <button class=\"check-button\">Ödemeyi Tamamla <i class=\"mdi mdi-chevron-right\"></i></button>
                </div>
            </div>
        </div>
    </div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "card/show_card.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 63,  160 => 56,  150 => 49,  140 => 42,  120 => 25,  114 => 22,  106 => 16,  97 => 15,  85 => 12,  80 => 11,  71 => 10,  58 => 8,  49 => 7,  39 => 1,  37 => 5,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% trans_default_domain 'card' %}

{% set active = 'card' %}

{% block page_title %}
    {{ 'card.show.card' | trans }}
{% endblock %}
{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" href={{ asset(\"css/styleCard.css\") }}>
{% endblock %}

{% block content %}
    <div class=\"main-content\">
        <div class=\"card-checkout\">
            <div class=\"left-content\">
                <ul class=\"list\">
                    <li>
                        <h5>Code</h5>
                        <h4>{{ card.cardCode }}</h4>
                    </li>
                    <li>
                        <h5>Date création</h5> <h4>{{ card.dateCreate | date('d/m/Y')}}</h4>
                    </li>
                    <li>
                        <h5></h5>
                        <h4><i class=\"mdi mdi-currency-try\"></i></h4>
                    </li>
                </ul>
                <button class=\"back-button\"> <i class=\"mdi mdi-chevron-left\"></i> Shinigami Laser</button>
            </div>
            <div class=\"right-content\">
                <div class=\"top-content\">
                    <h5>Carte de fidélité</h5><i class=\"mdi mdi-chevron-right\"></i>
                    <img class=\"card-logo\" src=\"https://image.flaticon.com/icons/svg/179/179449.svg\" />
                </div>
                <div class=\"middle-content\">
                    <div class=\"material-input-group\">
                        <i class=\"mdi mdi-account\"></i>
                        <input type=\"text\" class=\"material-input iconic\" value=\"{{ card.user.name }}\" required>
                        <span class=\"highlight\"></span>
                        <span class=\"material-input-bar\"></span>
                        <label class=\"material-input-label iconic\">Nom</label>
                    </div>
                    <div class=\"material-input-group\">
                        <i class=\"mdi mdi-credit-card\"></i>
                        <input type=\"text\" class=\"material-input iconic\" value=\"{{ card.user.username }}\" required>
                        <span class=\"highlight\"></span>
                        <span class=\"material-input-bar\"></span>
                        <label class=\"material-input-label iconic\">Prenom</label>
                    </div>
                    <div class=\"material-input-group date-input\">
                        <i class=\"mdi mdi-calendar-blank\"></i>
                        <input type=\"text\" class=\"material-input iconic\" value=\"{{ card.user.birthdate |  date('d/m/Y') }}\" required>
                        <span class=\"highlight\"></span>
                        <span class=\"material-input-bar\"></span>
                        <label class=\"material-input-label iconic\">Date de naissance </label>
                    </div>
                    <div class=\"material-input-group cvv-input\">
                        <i class=\"mdi mdi-numeric\"></i>
                        <input type=\"text\" class=\"material-input iconic\" value=\"{{ card.point }}\" required>
                        <span class=\"highlight\"></span>
                        <span class=\"material-input-bar\"></span>
                        <label class=\"material-input-label iconic\">Points</label>
                    </div>
                </div>
                <div class=\"bottom-content\">
                    <button class=\"check-button\">Ödemeyi Tamamla <i class=\"mdi mdi-chevron-right\"></i></button>
                </div>
            </div>
        </div>
    </div>


{% endblock %}

", "card/show_card.html.twig", "/Applications/MAMP/htdocs/shinigami_laser/templates/card/show_card.html.twig");
    }
}
