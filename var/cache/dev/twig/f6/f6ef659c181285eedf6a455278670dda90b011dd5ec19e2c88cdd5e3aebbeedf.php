<?php

/* match/list_scores.html.twig */
class __TwigTemplate_1b84f84ea151d82e8e3ec5a0dea352d788768a2ea923cee458641a71117ffdd4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "match/list_scores.html.twig", 1);
        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "match/list_scores.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "match/list_scores.html.twig"));

        // line 5
        $context["active"] = "score";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 7
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("scores.list", array(), "score"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/style_list.css"), "html", null, true);
        echo ">
    <link rel=\"stylesheet\" href=";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/countdown.demo.css"), "html", null, true);
        echo ">
    <style media=\"print\">
        table td {
            vertical-align: middle;
            background: #ffffff;
            border: 2px solid gray;
            border-right: 0;
            box-shadow: 0px 0px 0px 0px #eaeaea;
        }
    </style>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 26
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 27
        echo "    <div id=\"content\" class=\"txt-content\">
        ";
        // line 28
        if ((twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["matchPlayer"]) || array_key_exists("matchPlayer", $context) ? $context["matchPlayer"] : (function () { throw new Twig_Error_Runtime('Variable "matchPlayer" does not exist.', 28, $this->source); })()), "endDate", array()), "d/m/Y H:i:s") <= twig_date_format_filter($this->env, "now", "d/m/Y H:i:s"))) {
            // line 29
            echo "            <p>Votre score pour le match ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["matchPlayer"]) || array_key_exists("matchPlayer", $context) ? $context["matchPlayer"] : (function () { throw new Twig_Error_Runtime('Variable "matchPlayer" does not exist.', 29, $this->source); })()), "match", array()), "name", array()), "html", null, true);
            echo " :</p>
            <h2>";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["matchPlayer"]) || array_key_exists("matchPlayer", $context) ? $context["matchPlayer"] : (function () { throw new Twig_Error_Runtime('Variable "matchPlayer" does not exist.', 30, $this->source); })()), "score", array()), "html", null, true);
            echo "</h2>
        ";
        } else {
            // line 32
            echo "            <div>
                <h1>Vous avez ";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["matchPlayer"]) || array_key_exists("matchPlayer", $context) ? $context["matchPlayer"] : (function () { throw new Twig_Error_Runtime('Variable "matchPlayer" does not exist.', 33, $this->source); })()), "match", array()), "duration", array()), "html", null, true);
            echo " minutes à jouer.</h1>
                ";
            // line 34
            $context["difference"] = twig_get_attribute($this->env, $this->source, twig_date_converter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["matchPlayer"]) || array_key_exists("matchPlayer", $context) ? $context["matchPlayer"] : (function () { throw new Twig_Error_Runtime('Variable "matchPlayer" does not exist.', 34, $this->source); })()), "endDate", array())), "diff", array(0 => twig_date_converter($this->env, "now")), "method");
            // line 35
            echo "                <div>
                    <h3>
                        Il vous reste <time>00:";
            // line 37
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["difference"]) || array_key_exists("difference", $context) ? $context["difference"] : (function () { throw new Twig_Error_Runtime('Variable "difference" does not exist.', 37, $this->source); })()), "%i"), "html", null, true);
            echo ":";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["difference"]) || array_key_exists("difference", $context) ? $context["difference"] : (function () { throw new Twig_Error_Runtime('Variable "difference" does not exist.', 37, $this->source); })()), "%s"), "html", null, true);
            echo "</time> minutes à consulter votre score
                    </h3>
                </div>

            </div>
        ";
        }
        // line 43
        echo "        <p>Liste des scores : </p>

        <div id='tableView'>
            <table>
                <tr>
                    <th>";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("matchplayer.id", array(), "score"), "html", null, true);
        echo "</th>
                    <th>";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("match.name", array(), "score"), "html", null, true);
        echo "</th>
                    <th>";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("match.score", array(), "score"), "html", null, true);
        echo "</th>
                    <th>";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("match.createDate", array(), "score"), "html", null, true);
        echo "</th>
                </tr>
                ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["matchPlayers"]) || array_key_exists("matchPlayers", $context) ? $context["matchPlayers"] : (function () { throw new Twig_Error_Runtime('Variable "matchPlayers" does not exist.', 53, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["matchPlayer2"]) {
            // line 54
            echo "                    <tr>
                        <td>";
            // line 55
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["matchPlayer2"], "id", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 56
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["matchPlayer2"], "match", array()), "name", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 57
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["matchPlayer2"], "score", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 58
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["matchPlayer2"], "createdDate", array()), "d/m/Y"), "html", null, true);
            echo "</td>

                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['matchPlayer2'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "            </table>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 66
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 67
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>
    <script src=";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/jquery.countdown.js"), "html", null, true);
        echo "></script>
    <script>
        window.jQuery(function (\$) {
            \"use strict\";

            \$('time').countDown({
                with_separators: false
            });
            \$('.alt-1').countDown({
                css_class: 'countdown-alt-1'
            });
            \$('.alt-2').countDown({
                css_class: 'countdown-alt-2'
            });

        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "match/list_scores.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  241 => 69,  235 => 67,  226 => 66,  213 => 62,  203 => 58,  199 => 57,  195 => 56,  191 => 55,  188 => 54,  184 => 53,  179 => 51,  175 => 50,  171 => 49,  167 => 48,  160 => 43,  149 => 37,  145 => 35,  143 => 34,  139 => 33,  136 => 32,  131 => 30,  126 => 29,  124 => 28,  121 => 27,  112 => 26,  90 => 13,  86 => 12,  81 => 11,  72 => 10,  59 => 8,  50 => 7,  40 => 1,  38 => 5,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% trans_default_domain 'score' %}

{% set active = 'score' %}

{% block page_title %}
    {{ 'scores.list' | trans }}
{% endblock %}
{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" href={{ asset(\"css/style_list.css\") }}>
    <link rel=\"stylesheet\" href={{ asset(\"css/countdown.demo.css\") }}>
    <style media=\"print\">
        table td {
            vertical-align: middle;
            background: #ffffff;
            border: 2px solid gray;
            border-right: 0;
            box-shadow: 0px 0px 0px 0px #eaeaea;
        }
    </style>

{% endblock %}

{% block content %}
    <div id=\"content\" class=\"txt-content\">
        {% if matchPlayer.endDate | date('d/m/Y H:i:s') <= \"now\" | date('d/m/Y H:i:s') %}
            <p>Votre score pour le match {{ matchPlayer.match.name }} :</p>
            <h2>{{ matchPlayer.score }}</h2>
        {% else %}
            <div>
                <h1>Vous avez {{ matchPlayer.match.duration }} minutes à jouer.</h1>
                {% set difference = (date(matchPlayer.endDate).diff(date(\"now\"))) %}
                <div>
                    <h3>
                        Il vous reste <time>00:{{ difference | date(\"%i\") }}:{{ difference | date(\"%s\") }}</time> minutes à consulter votre score
                    </h3>
                </div>

            </div>
        {% endif %}
        <p>Liste des scores : </p>

        <div id='tableView'>
            <table>
                <tr>
                    <th>{{ 'matchplayer.id' | trans }}</th>
                    <th>{{ 'match.name' | trans }}</th>
                    <th>{{ 'match.score' | trans }}</th>
                    <th>{{ 'match.createDate' | trans }}</th>
                </tr>
                {% for matchPlayer2 in matchPlayers %}
                    <tr>
                        <td>{{ matchPlayer2.id }}</td>
                        <td>{{ matchPlayer2.match.name }}</td>
                        <td>{{ matchPlayer2.score }}</td>
                        <td>{{ matchPlayer2.createdDate | date('d/m/Y') }}</td>

                    </tr>
                {% endfor %}
            </table>
        </div>
    </div>
{% endblock %}
{% block javascripts %}
    {{ parent() }}
    <script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>
    <script src={{ asset(\"js/jquery.countdown.js\")}}></script>
    <script>
        window.jQuery(function (\$) {
            \"use strict\";

            \$('time').countDown({
                with_separators: false
            });
            \$('.alt-1').countDown({
                css_class: 'countdown-alt-1'
            });
            \$('.alt-2').countDown({
                css_class: 'countdown-alt-2'
            });

        });
    </script>
{% endblock %}", "match/list_scores.html.twig", "/Applications/MAMP/htdocs/shinigami_laser/templates/match/list_scores.html.twig");
    }
}
