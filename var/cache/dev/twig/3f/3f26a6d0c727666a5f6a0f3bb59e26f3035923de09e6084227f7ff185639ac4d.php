<?php

/* index/index.html.twig */
class __TwigTemplate_7839914bc732a9836bf4d1fae24b283f04d4e075d9c633e3497573562ebf2445 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "index/index.html.twig", 1);
        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "index/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "index/index.html.twig"));

        // line 5
        $context["active"] = "home";
        // line 7
        $context["imageLink"] = "http://localhost/TD/symfony/shinigami_laser";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        // line 10
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("homePage.home", array(), "homePage"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 15
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/styleHomePage.css"), "html", null, true);
        echo ">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 19
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 20
        echo "    <div class=\"hero-circles\">
        <div class=\"row\">
            <h1> Le plus beau lasergame de <i>Paris...</i></h1>
            <div class=\"mobile-slider\"></div>
            <ul>
                <li>
                    <span class=\"border\"></span>
                    <div class=\"discover circle\" data-name=\"discover\">
                        <div class=\"bg\" style=\"background-image:url('";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["imageLink"]) || array_key_exists("imageLink", $context) ? $context["imageLink"] : (function () { throw new Twig_Error_Runtime('Variable "imageLink" does not exist.', 28, $this->source); })()), "html", null, true);
        echo "/public/images/homePage/laserworld30.jpg')\" data-bg=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/homePage/laserworld30.jpg"), "html", null, true);
        echo "\"></div>
                    </div>
                    <p class=\"tagline first\">
                        <span class=\"title closed\">Exit</span>
                        <span class=\"title open\">Venez-vous affronter</span>
                        <span class=\"subtitle\">par équipe <span>ou</span> en solo</span>
                    </p>
                </li>
                <li>
                    <span class=\"border\"></span>
                    <div class=\"create circle\" data-name=\"create\">
                        <div class=\"bg\" style=\"background-image:url('";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["imageLink"]) || array_key_exists("imageLink", $context) ? $context["imageLink"] : (function () { throw new Twig_Error_Runtime('Variable "imageLink" does not exist.', 39, $this->source); })()), "html", null, true);
        echo "/public/images/homePage/laserworld1.jpg')\" data-bg=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/homePage/laserworld1.jpg"), "html", null, true);
        echo "\"></div>
                    </div>
                    <p class=\"tagline second\">
                        <span class=\"title closed\">World</span>
                        <span class=\"title open\">Une immersion totale qui vous procurera </span>
                        <span class=\"subtitle\">une expérience <span>de</span> jeu unique</span>
                    </p>
                </li>
                <li>
                    <span class=\"border\"></span>
                    <div class=\"evolve circle\" data-name=\"evolve\">
                        <div class=\"bg\" style=\"background-image:url('http://localhost/TD/symfony/shinigami_laser/public/images/homePage/laserworld18.jpg')\" data-bg=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/homePage/laserworld18.jpg"), "html", null, true);
        echo "\"></div>
                    </div>
                    <p class=\"tagline third\">
                        <span class=\"title closed\">Demonia</span>
                        <span class=\"title open\">Dans un labyrinthe aux décors </span>
                        <span class=\"subtitle\">stupéfiants <span>et</span> époustouflant</span>
                    </p>
                </li>
            </ul>
        </div>
        <div class=\"circle-expander\">
            <span class=\"bg\"></span>
            <div class=\"title-overlay\"></div>
            <a href=\"#\" class=\"close-btn\">Close</a>
            <div class=\"expander-nav\">
                <a href=\"#\" class=\"discover\">Exit</a>
                <a href=\"#\" class=\"create\">World</a>
                <a href=\"#\" class=\"evolve\">Demonia</a>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "index/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 50,  134 => 39,  118 => 28,  108 => 20,  99 => 19,  87 => 16,  82 => 15,  73 => 14,  60 => 10,  51 => 9,  41 => 1,  39 => 7,  37 => 5,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% trans_default_domain 'homePage' %}

{% set active = 'home' %}

{% set imageLink = 'http://localhost/TD/symfony/shinigami_laser' %}

{% block page_title %}
    {{ 'homePage.home' | trans }}
{% endblock %}


{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" href={{ asset(\"css/styleHomePage.css\") }}>
{% endblock %}

{% block content %}
    <div class=\"hero-circles\">
        <div class=\"row\">
            <h1> Le plus beau lasergame de <i>Paris...</i></h1>
            <div class=\"mobile-slider\"></div>
            <ul>
                <li>
                    <span class=\"border\"></span>
                    <div class=\"discover circle\" data-name=\"discover\">
                        <div class=\"bg\" style=\"background-image:url('{{ imageLink }}/public/images/homePage/laserworld30.jpg')\" data-bg=\"{{ asset('images/homePage/laserworld30.jpg') }}\"></div>
                    </div>
                    <p class=\"tagline first\">
                        <span class=\"title closed\">Exit</span>
                        <span class=\"title open\">Venez-vous affronter</span>
                        <span class=\"subtitle\">par équipe <span>ou</span> en solo</span>
                    </p>
                </li>
                <li>
                    <span class=\"border\"></span>
                    <div class=\"create circle\" data-name=\"create\">
                        <div class=\"bg\" style=\"background-image:url('{{ imageLink }}/public/images/homePage/laserworld1.jpg')\" data-bg=\"{{ asset('images/homePage/laserworld1.jpg') }}\"></div>
                    </div>
                    <p class=\"tagline second\">
                        <span class=\"title closed\">World</span>
                        <span class=\"title open\">Une immersion totale qui vous procurera </span>
                        <span class=\"subtitle\">une expérience <span>de</span> jeu unique</span>
                    </p>
                </li>
                <li>
                    <span class=\"border\"></span>
                    <div class=\"evolve circle\" data-name=\"evolve\">
                        <div class=\"bg\" style=\"background-image:url('http://localhost/TD/symfony/shinigami_laser/public/images/homePage/laserworld18.jpg')\" data-bg=\"{{ asset('images/homePage/laserworld18.jpg') }}\"></div>
                    </div>
                    <p class=\"tagline third\">
                        <span class=\"title closed\">Demonia</span>
                        <span class=\"title open\">Dans un labyrinthe aux décors </span>
                        <span class=\"subtitle\">stupéfiants <span>et</span> époustouflant</span>
                    </p>
                </li>
            </ul>
        </div>
        <div class=\"circle-expander\">
            <span class=\"bg\"></span>
            <div class=\"title-overlay\"></div>
            <a href=\"#\" class=\"close-btn\">Close</a>
            <div class=\"expander-nav\">
                <a href=\"#\" class=\"discover\">Exit</a>
                <a href=\"#\" class=\"create\">World</a>
                <a href=\"#\" class=\"evolve\">Demonia</a>
            </div>
        </div>
    </div>
{% endblock %}
", "index/index.html.twig", "/Applications/MAMP/htdocs/shinigami_laser/templates/index/index.html.twig");
    }
}
