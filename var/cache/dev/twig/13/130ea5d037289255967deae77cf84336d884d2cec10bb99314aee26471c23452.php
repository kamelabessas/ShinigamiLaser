<?php

/* club/index.html.twig */
class __TwigTemplate_9b6e37506b33ac88f826c478c013daaa918573ede555de5e6d74d02203ea63a8 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "club/index.html.twig", 1);
        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "club/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "club/index.html.twig"));

        // line 5
        $context["active"] = "establishments";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 7
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("club.list", array(), "club"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 12
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/style_list.css"), "html", null, true);
        echo ">
    <style media=\"print\">
        table td {
            vertical-align: middle;
            background: #ffffff;
            border: 2px solid gray;
            border-right: 0;
            box-shadow: 0px 0px 0px 0px #eaeaea;
        }
    </style>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 25
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 26
        echo "

    ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 28, $this->source); })()), "flashes", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 29
            echo "        <div class=\"alert alert-danger\">
            ";
            // line 30
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "    <div id='tableView'>
        <table>
            ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["establishments"]) || array_key_exists("establishments", $context) ? $context["establishments"] : (function () { throw new Twig_Error_Runtime('Variable "establishments" does not exist.', 35, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["establishment"]) {
            // line 36
            echo "                <tr>
                    <td>";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["establishment"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["establishment"], "establishmentName", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["establishment"], "address", array()), "html", null, true);
            echo "</td>
                    <td width=\"40%\">
                        <div class=\"col-md-3 col-sm-4 col-xs-12\">
                            <a href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edit_establishment", array("id" => twig_get_attribute($this->env, $this->source, $context["establishment"], "id", array()))), "html", null, true);
            echo "\"
                               class=\"btn btn-default\">
                                ";
            // line 44
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("club.edit", array(), "club"), "html", null, true);
            echo "
                            </a>
                        </div>
                        <div class=\"col-md-5 col-sm-4 col-xs-12\">
                            <a href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("show_establishment", array("id" => twig_get_attribute($this->env, $this->source, $context["establishment"], "id", array()))), "html", null, true);
            echo "\"
                               class=\"btn btn-default\">
                                ";
            // line 50
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("club.read.more", array(), "club"), "html", null, true);
            echo "
                                <i class=\"glyphicon glyphicon-chevron-right\"></i>
                            </a>
                        </div>
                        <div class=\"col-md-4 col-sm-4 col-xs-12\">
                            <form action=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("delete_establishment", array("id" => twig_get_attribute($this->env, $this->source, $context["establishment"], "id", array()))), "html", null, true);
            echo "\" method=\"post\"
                                  data-confirmation=\"true\" id=\"delete-form\">
                                <input type=\"hidden\" name=\"token\" value=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("delete"), "html", null, true);
            echo "\"/>
                                <button type=\"submit\" class=\"btn btn-default btn-danger\">
                                    ";
            // line 59
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("club.delete", array(), "club"), "html", null, true);
            echo "
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['establishment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "        </table>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "club/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 66,  196 => 59,  191 => 57,  186 => 55,  178 => 50,  173 => 48,  166 => 44,  161 => 42,  155 => 39,  151 => 38,  147 => 37,  144 => 36,  140 => 35,  136 => 33,  127 => 30,  124 => 29,  120 => 28,  116 => 26,  107 => 25,  85 => 13,  80 => 12,  71 => 11,  58 => 8,  49 => 7,  39 => 1,  37 => 5,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% trans_default_domain 'club' %}

{% set active = 'establishments' %}

{% block page_title %}
    {{ 'club.list' | trans }}
{% endblock %}

{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" href={{ asset(\"css/style_list.css\") }}>
    <style media=\"print\">
        table td {
            vertical-align: middle;
            background: #ffffff;
            border: 2px solid gray;
            border-right: 0;
            box-shadow: 0px 0px 0px 0px #eaeaea;
        }
    </style>

{% endblock %}
{% block content %}


    {% for message in app.flashes('success') %}
        <div class=\"alert alert-danger\">
            {{ message }}
        </div>
    {% endfor %}
    <div id='tableView'>
        <table>
            {% for establishment in establishments %}
                <tr>
                    <td>{{ establishment.id }}</td>
                    <td>{{ establishment.establishmentName }}</td>
                    <td>{{ establishment.address }}</td>
                    <td width=\"40%\">
                        <div class=\"col-md-3 col-sm-4 col-xs-12\">
                            <a href=\"{{ path('edit_establishment',{'id':establishment.id}) }}\"
                               class=\"btn btn-default\">
                                {{ 'club.edit' | trans }}
                            </a>
                        </div>
                        <div class=\"col-md-5 col-sm-4 col-xs-12\">
                            <a href=\"{{ path('show_establishment', {'id': establishment.id}) }}\"
                               class=\"btn btn-default\">
                                {{ 'club.read.more' | trans }}
                                <i class=\"glyphicon glyphicon-chevron-right\"></i>
                            </a>
                        </div>
                        <div class=\"col-md-4 col-sm-4 col-xs-12\">
                            <form action=\"{{ url('delete_establishment', {id: establishment.id}) }}\" method=\"post\"
                                  data-confirmation=\"true\" id=\"delete-form\">
                                <input type=\"hidden\" name=\"token\" value=\"{{ csrf_token('delete') }}\"/>
                                <button type=\"submit\" class=\"btn btn-default btn-danger\">
                                    {{ 'club.delete'|trans }}
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
            {% endfor %}
        </table>
    </div>
{% endblock %}
", "club/index.html.twig", "/Applications/MAMP/htdocs/shinigami_laser/templates/club/index.html.twig");
    }
}
