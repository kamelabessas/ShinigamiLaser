<?php

/* User/show_user.html.twig */
class __TwigTemplate_2ee30d752c746bfe6329cd0b7b614abe5edb1eccf6c550fec6f6e8f348bda81e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "User/show_user.html.twig", 1);
        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "User/show_user.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "User/show_user.html.twig"));

        // line 5
        $context["active"] = "show";
        // line 7
        $context["imageLink"] = "http://localhost/TD/symfony/shinigami_laser";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        // line 10
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("showUser", array(), "showUser"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 15
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/styleShowUser.css"), "html", null, true);
        echo ">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 19
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 20
        echo "    <div class=\"main-content\">
        <div class=\"container\">
            <h3 class=\"site-title\">Mon profil</h3>
            <div class=\"row\">
                <div class=\"col-md-9\">
                    <div class=\"row\">
                        <div class=\"panel\">
                            <div class=\" panel-body \">

                                <div class=\"col-md-5\">
                                    <img class=\"img-responsive\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("images/user/" . twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 30, $this->source); })()), "picture", array()))), "html", null, true);
        echo "\" style=\"width:50%;\">
                                    <div class=\" clearfix\">
                                        <h3>";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 32, $this->source); })()), "username", array()), "html", null, true);
        echo "</h3>
                                        <h4>";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 33, $this->source); })()), "address", array()), "html", null, true);
        echo "</h4>
                                        <div class=\"col-md-5 col-sm-4 col-xs-12\">
                                            <a href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edit_user", array("id" => twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 35, $this->source); })()), "id", array()))), "html", null, true);
        echo "\"
                                               class=\"btn btn-default\">
                                                ";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("user.edit.user", array(), "showUser"), "html", null, true);
        echo "
                                                <i class=\"glyphicon glyphicon-chevron-right\"></i>
                                            </a>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class=\"col-md-7\">
                                    <div class=\"profile-block\">
                                        <ul class=\"list-group\">
                                            <li class=\"list-group-item\">Code d'utilisateur <span class=\"badge\">";
        // line 47
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 47, $this->source); })()), "userCode", array()), "html", null, true);
        echo "</span></li>
                                            <li class=\"list-group-item\">Nom <span class=\"badge\">";
        // line 48
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 48, $this->source); })()), "name", array()), "html", null, true);
        echo "</span></li>
                                            <li class=\"list-group-item\">Nom utilisateur<span class=\"badge\">";
        // line 49
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 49, $this->source); })()), "username", array()), "html", null, true);
        echo "</span></li>
                                            <li class=\"list-group-item\">Email <span class=\"badge\">";
        // line 50
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 50, $this->source); })()), "email", array()), "html", null, true);
        echo "</span></li>
                                            <li class=\"list-group-item\">Date de naissance <span class=\"badge\">";
        // line 51
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 51, $this->source); })()), "birthdate", array()), "d/m/Y"), "html", null, true);
        echo "</span></li>
                                            <li class=\"list-group-item\">Club<span class=\"badge\">club</span></li>
                                         </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "User/show_user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 51,  164 => 50,  160 => 49,  156 => 48,  152 => 47,  139 => 37,  134 => 35,  129 => 33,  125 => 32,  120 => 30,  108 => 20,  99 => 19,  87 => 16,  82 => 15,  73 => 14,  60 => 10,  51 => 9,  41 => 1,  39 => 7,  37 => 5,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% trans_default_domain 'showUser' %}

{% set active = 'show' %}

{% set imageLink = 'http://localhost/TD/symfony/shinigami_laser' %}

{% block page_title %}
    {{ 'showUser' | trans }}
{% endblock %}


{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" href={{ asset(\"css/styleShowUser.css\") }}>
{% endblock %}

{% block content %}
    <div class=\"main-content\">
        <div class=\"container\">
            <h3 class=\"site-title\">Mon profil</h3>
            <div class=\"row\">
                <div class=\"col-md-9\">
                    <div class=\"row\">
                        <div class=\"panel\">
                            <div class=\" panel-body \">

                                <div class=\"col-md-5\">
                                    <img class=\"img-responsive\" src=\"{{ asset('images/user/'~user.picture) }}\" style=\"width:50%;\">
                                    <div class=\" clearfix\">
                                        <h3>{{ user.username }}</h3>
                                        <h4>{{ user.address }}</h4>
                                        <div class=\"col-md-5 col-sm-4 col-xs-12\">
                                            <a href=\"{{ path('edit_user', {id: user.id})}}\"
                                               class=\"btn btn-default\">
                                                {{ 'user.edit.user' | trans }}
                                                <i class=\"glyphicon glyphicon-chevron-right\"></i>
                                            </a>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class=\"col-md-7\">
                                    <div class=\"profile-block\">
                                        <ul class=\"list-group\">
                                            <li class=\"list-group-item\">Code d'utilisateur <span class=\"badge\">{{ user.userCode }}</span></li>
                                            <li class=\"list-group-item\">Nom <span class=\"badge\">{{ user.name }}</span></li>
                                            <li class=\"list-group-item\">Nom utilisateur<span class=\"badge\">{{ user.username }}</span></li>
                                            <li class=\"list-group-item\">Email <span class=\"badge\">{{ user.email }}</span></li>
                                            <li class=\"list-group-item\">Date de naissance <span class=\"badge\">{{ user.birthdate | date('d/m/Y') }}</span></li>
                                            <li class=\"list-group-item\">Club<span class=\"badge\">club</span></li>
                                         </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
{% endblock %}
", "User/show_user.html.twig", "/Applications/MAMP/htdocs/shinigami_laser/templates/User/show_user.html.twig");
    }
}
