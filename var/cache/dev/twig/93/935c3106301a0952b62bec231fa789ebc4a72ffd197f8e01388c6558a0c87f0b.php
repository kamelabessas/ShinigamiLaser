<?php

/* components/_nav.html.twig */
class __TwigTemplate_50e24cab8d4beb9eac9574fc17631363129c51d466ab64a22164c184ec73561b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "components/_nav.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "components/_nav.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "<header class=\"header\">
    <div class=\"row\">
        <div class=\"col-md-3 col-sm-4 col-xs-12\">
            <a href=\"";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("index");
        echo "\" class=\"logo\">
                <img alt=\"Logo\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("images/logo.png"), "html", null, true);
        echo "\" width=\"150\"/>
            </a>
        </div>
    </div>
</header>
<nav class=\"navbar navbar-inverse\" role=\"navigation\">
    <div class=\"container-fluid\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\"
                    data-target=\"#bs-example-navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
        </div>

        ";
        // line 26
        echo "        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav\">
                <li ";
        // line 28
        if (((isset($context["active"]) || array_key_exists("active", $context) ? $context["active"] : (function () { throw new Twig_Error_Runtime('Variable "active" does not exist.', 28, $this->source); })()) == "home")) {
            echo "class=\"active\"";
        }
        echo ">
                    <a href=\"";
        // line 29
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("index");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.home", array(), "navigation"), "html", null, true);
        echo " </a>
                </li>
                <li ";
        // line 31
        if (((isset($context["active"]) || array_key_exists("active", $context) ? $context["active"] : (function () { throw new Twig_Error_Runtime('Variable "active" does not exist.', 31, $this->source); })()) == "games")) {
            echo "class=\"active\"";
        }
        echo ">
                    <a href=\"";
        // line 32
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("index_game");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.games", array(), "navigation"), "html", null, true);
        echo "</a>
                </li>
                ";
        // line 34
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 35
            echo "                <li ";
            if (((isset($context["active"]) || array_key_exists("active", $context) ? $context["active"] : (function () { throw new Twig_Error_Runtime('Variable "active" does not exist.', 35, $this->source); })()) == "games")) {
                echo "class=\"active\"";
            }
            echo ">
                    <a href=\"";
            // line 36
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("matches_to_play_action");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.play.match", array(), "navigation"), "html", null, true);
            echo "</a>
                </li>
                ";
        }
        // line 39
        echo "            </ul>

            <ul class=\"nav navbar-nav navbar-right\">
                ";
        // line 42
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 43
            echo "                    <li ";
            if (((isset($context["active"]) || array_key_exists("active", $context) ? $context["active"] : (function () { throw new Twig_Error_Runtime('Variable "active" does not exist.', 43, $this->source); })()) == "logout")) {
                echo "class=\"active\"";
            }
            echo ">
                        <a href=\"";
            // line 44
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("deconnexion");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.logout", array(), "navigation"), "html", null, true);
            echo "</a>
                    </li>
                ";
        } else {
            // line 47
            echo "                    <li ";
            if (((isset($context["active"]) || array_key_exists("active", $context) ? $context["active"] : (function () { throw new Twig_Error_Runtime('Variable "active" does not exist.', 47, $this->source); })()) == "login")) {
                echo "class=\"active\"";
            }
            echo ">
                        <a href=\"";
            // line 48
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("login");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.login", array(), "navigation"), "html", null, true);
            echo "</a>
                    </li>

                    <li ";
            // line 51
            if (((isset($context["active"]) || array_key_exists("active", $context) ? $context["active"] : (function () { throw new Twig_Error_Runtime('Variable "active" does not exist.', 51, $this->source); })()) == "register")) {
                echo "class=\"active\"";
            }
            echo ">
                        <a href=\"";
            // line 52
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("register");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.register", array(), "navigation"), "html", null, true);
            echo "</a>
                    </li>
                ";
        }
        // line 55
        echo "                ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 56
            echo "                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                        ";
            // line 58
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.admin", array(), "navigation"), "html", null, true);
            echo "<b class=\"caret\"></b>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <li><a href=\"#\">";
            // line 61
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.list.user", array(), "navigation"), "html", null, true);
            echo "</a></li>
                        <li><a href=\"#\">";
            // line 62
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.add.user", array(), "navigation"), "html", null, true);
            echo "</a></li>
                        <li class=\"divider\"></li>
                        <li><a href=\"";
            // line 64
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("index_establishment");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.list.establishment", array(), "navigation"), "html", null, true);
            echo "</a></li>
                        <li><a href=\"";
            // line 65
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("add_establishment");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.add.establishment", array(), "navigation"), "html", null, true);
            echo "</a></li>
                        <li><a href=\"";
            // line 66
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("add_game");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.add.game", array(), "navigation"), "html", null, true);
            echo "</a></li>
                        <li><a href=\"";
            // line 67
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("index_match");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.list.match", array(), "navigation"), "html", null, true);
            echo "</a></li>
                        <li><a href=\"";
            // line 68
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("add_match");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.add.match", array(), "navigation"), "html", null, true);
            echo "</a></li>
                    </ul>
                </li>
                ";
        }
        // line 72
        echo "
                <li ";
        // line 73
        if (((isset($context["active"]) || array_key_exists("active", $context) ? $context["active"] : (function () { throw new Twig_Error_Runtime('Variable "active" does not exist.', 73, $this->source); })()) == "basket")) {
            echo "class=\"active\"";
        }
        echo ">
                    <a href=\"";
        // line 74
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("basket_action");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("nav.basket", array(), "navigation"), "html", null, true);
        echo "</a>
                </li>
            </ul>
        </div>
        ";
        // line 79
        echo "    </div>
    ";
        // line 81
        echo "</nav>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "components/_nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 81,  232 => 79,  223 => 74,  217 => 73,  214 => 72,  205 => 68,  199 => 67,  193 => 66,  187 => 65,  181 => 64,  176 => 62,  172 => 61,  166 => 58,  162 => 56,  159 => 55,  151 => 52,  145 => 51,  137 => 48,  130 => 47,  122 => 44,  115 => 43,  113 => 42,  108 => 39,  100 => 36,  93 => 35,  91 => 34,  84 => 32,  78 => 31,  71 => 29,  65 => 28,  61 => 26,  41 => 8,  37 => 7,  32 => 4,  29 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'navigation' %}

{#header#}
<header class=\"header\">
    <div class=\"row\">
        <div class=\"col-md-3 col-sm-4 col-xs-12\">
            <a href=\"{{ url('index') }}\" class=\"logo\">
                <img alt=\"Logo\" src=\"{{ asset('images/logo.png') }}\" width=\"150\"/>
            </a>
        </div>
    </div>
</header>
<nav class=\"navbar navbar-inverse\" role=\"navigation\">
    <div class=\"container-fluid\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\"
                    data-target=\"#bs-example-navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
        </div>

        {#Collect the nav links, forms, and other content for toggling#}
        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav\">
                <li {% if(active == 'home') %}class=\"active\"{%endif%}>
                    <a href=\"{{ url('index') }}\">{{ 'nav.home' | trans }} </a>
                </li>
                <li {% if(active == 'games') %}class=\"active\"{%endif%}>
                    <a href=\"{{ url('index_game') }}\">{{ 'nav.games' | trans }}</a>
                </li>
                {% if is_granted('IS_AUTHENTICATED_FULLY') %}
                <li {% if(active == 'games') %}class=\"active\"{%endif%}>
                    <a href=\"{{ url('matches_to_play_action') }}\">{{ 'nav.play.match' | trans }}</a>
                </li>
                {% endif %}
            </ul>

            <ul class=\"nav navbar-nav navbar-right\">
                {% if is_granted('IS_AUTHENTICATED_FULLY') %}
                    <li {% if(active == 'logout') %}class=\"active\"{%endif%}>
                        <a href=\"{{ url('deconnexion') }}\">{{ 'nav.logout' | trans }}</a>
                    </li>
                {% else %}
                    <li {% if(active == 'login') %}class=\"active\"{%endif%}>
                        <a href=\"{{ url('login') }}\">{{ 'nav.login' | trans }}</a>
                    </li>

                    <li {% if(active == 'register') %}class=\"active\"{%endif%}>
                        <a href=\"{{ url('register') }}\">{{ 'nav.register' | trans }}</a>
                    </li>
                {% endif %}
                {% if is_granted('ROLE_ADMIN') %}
                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                        {{ 'nav.admin' | trans }}<b class=\"caret\"></b>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <li><a href=\"#\">{{ 'nav.list.user' | trans }}</a></li>
                        <li><a href=\"#\">{{ 'nav.add.user' | trans }}</a></li>
                        <li class=\"divider\"></li>
                        <li><a href=\"{{ url('index_establishment') }}\">{{ 'nav.list.establishment' | trans }}</a></li>
                        <li><a href=\"{{ url('add_establishment') }}\">{{ 'nav.add.establishment' | trans }}</a></li>
                        <li><a href=\"{{ url('add_game') }}\">{{ 'nav.add.game' | trans }}</a></li>
                        <li><a href=\"{{ url('index_match') }}\">{{ 'nav.list.match' | trans }}</a></li>
                        <li><a href=\"{{ url('add_match') }}\">{{ 'nav.add.match' | trans }}</a></li>
                    </ul>
                </li>
                {% endif %}

                <li {% if(active == 'basket') %}class=\"active\"{%endif%}>
                    <a href=\"{{ url('basket_action') }}\">{{ 'nav.basket' | trans }}</a>
                </li>
            </ul>
        </div>
        {#/.navbar-collapse#}
    </div>
    {#/.container-fluid#}
</nav>", "components/_nav.html.twig", "/Applications/MAMP/htdocs/shinigami_laser/templates/components/_nav.html.twig");
    }
}
