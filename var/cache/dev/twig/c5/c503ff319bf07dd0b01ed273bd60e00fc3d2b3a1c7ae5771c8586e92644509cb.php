<?php

/* basket/userBasket.html.twig */
class __TwigTemplate_56e13e08d8f7942d7052128936bb3b2cb58a9030979d9845d577bd1a1be6aeed extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "basket/userBasket.html.twig", 1);
        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "basket/userBasket.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "basket/userBasket.html.twig"));

        // line 5
        $context["active"] = "matchs";
        // line 6
        $context["totalHT"] = 0;
        // line 7
        $context["totalTTC"] = 0;
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        // line 10
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("basket.list", array(), "basket"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/style_list.css"), "html", null, true);
        echo ">
    <style media=\"print\">
        table td {
            vertical-align: middle;
            background: #ffffff;
            border: 2px solid gray;
            border-right: 0;
            box-shadow: 0px 0px 0px 0px #eaeaea;
        }
    </style>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 27
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 28
        echo "
    ";
        // line 29
        if (twig_test_empty((isset($context["basket"]) || array_key_exists("basket", $context) ? $context["basket"] : (function () { throw new Twig_Error_Runtime('Variable "basket" does not exist.', 29, $this->source); })()))) {
            // line 30
            echo "        <h1>Votre panier est vide pour le moment.</h1>
    ";
        } else {
            // line 32
            echo "        <div id='tableView'>
            <table>
                <tr>
                    <th>";
            // line 35
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("basket.name", array(), "basket"), "html", null, true);
            echo "</th>
                    <th>";
            // line 36
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("basket.players.number", array(), "basket"), "html", null, true);
            echo "</th>
                    <th>";
            // line 37
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("basket.price", array(), "basket"), "html", null, true);
            echo "</th>
                    <th>";
            // line 38
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("basket.total.ht.price", array(), "basket"), "html", null, true);
            echo "</th>
                    <th>";
            // line 39
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("basket.delete", array(), "basket"), "html", null, true);
            echo "</th>
                </tr>
                ";
            // line 41
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["matches"]) || array_key_exists("matches", $context) ? $context["matches"] : (function () { throw new Twig_Error_Runtime('Variable "matches" does not exist.', 41, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["match"]) {
                // line 42
                echo "                    <tr>

                        <td>";
                // line 44
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["match"], "name", array()), "html", null, true);
                echo "</td>
                        <td>
                            <form action=\"";
                // line 46
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("participate_action", array("id" => twig_get_attribute($this->env, $this->source, $context["match"], "id", array()))), "html", null, true);
                echo "\" method=\"get\">
                                <select class=\"span1\" name=\"qte\" onchange=\"this.form.submit()\">
                                    ";
                // line 48
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(twig_get_attribute($this->env, $this->source, $context["match"], "minNbPlayer", array()), twig_get_attribute($this->env, $this->source, $context["match"], "maxNbPlayer", array())));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 49
                    echo "                                        <option value=\"";
                    echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                    echo "\" ";
                    if (($context["i"] == twig_get_attribute($this->env, $this->source, (isset($context["basket"]) || array_key_exists("basket", $context) ? $context["basket"] : (function () { throw new Twig_Error_Runtime('Variable "basket" does not exist.', 49, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["match"], "id", array()), array(), "array"))) {
                        echo " selected=selected ";
                    }
                    echo ">";
                    echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                    echo "</option>
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 51
                echo "                                </select>
                            </form>
                        </td>
                        <td>";
                // line 54
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["match"], "game", array()), "price", array()), "html", null, true);
                echo "</td>
                        <td>";
                // line 55
                echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["match"], "game", array()), "price", array()) * twig_get_attribute($this->env, $this->source, (isset($context["basket"]) || array_key_exists("basket", $context) ? $context["basket"] : (function () { throw new Twig_Error_Runtime('Variable "basket" does not exist.', 55, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["match"], "id", array()), array(), "array")), "html", null, true);
                echo " €</td>
                        <td><a href=\"";
                // line 56
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("basket_delete", array("id" => twig_get_attribute($this->env, $this->source, $context["match"], "id", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("basket.delete", array(), "basket"), "html", null, true);
                echo "</a>
                        </td>
                    </tr>
                    ";
                // line 59
                $context["totalHT"] = ((isset($context["totalHT"]) || array_key_exists("totalHT", $context) ? $context["totalHT"] : (function () { throw new Twig_Error_Runtime('Variable "totalHT" does not exist.', 59, $this->source); })()) + (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["match"], "game", array()), "price", array()) * twig_get_attribute($this->env, $this->source, (isset($context["basket"]) || array_key_exists("basket", $context) ? $context["basket"] : (function () { throw new Twig_Error_Runtime('Variable "basket" does not exist.', 59, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["match"], "id", array()), array(), "array")));
                // line 60
                echo "                    ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 60, $this->source); })()), "session", array()), "set", array(0 => "totalHT", 1 => (isset($context["totalHT"]) || array_key_exists("totalHT", $context) ? $context["totalHT"] : (function () { throw new Twig_Error_Runtime('Variable "totalHT" does not exist.', 60, $this->source); })())), "method"), "html", null, true);
                echo "
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['match'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 62
            echo "            </table>
            <dl class=\"pull-right\">
                <dt>Total :</dt>
                <dd>";
            // line 65
            echo twig_escape_filter($this->env, (isset($context["totalHT"]) || array_key_exists("totalHT", $context) ? $context["totalHT"] : (function () { throw new Twig_Error_Runtime('Variable "totalHT" does not exist.', 65, $this->source); })()), "html", null, true);
            echo "</dd>
            </dl>
            ";
            // line 67
            if ( !twig_test_empty((isset($context["basket"]) || array_key_exists("basket", $context) ? $context["basket"] : (function () { throw new Twig_Error_Runtime('Variable "basket" does not exist.', 67, $this->source); })()))) {
                // line 68
                echo "                ";
                if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_FULLY")) {
                    // line 69
                    echo "                    <a href=\"";
                    echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("payment_form_action");
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("basket.payment", array(), "basket"), "html", null, true);
                    echo "</a>
                ";
                } else {
                    // line 71
                    echo "                    <a href=\"";
                    echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("login");
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("basket.payment", array(), "basket"), "html", null, true);
                    echo "</a>
                ";
                }
                // line 73
                echo "            ";
            }
            // line 74
            echo "        </div>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "basket/userBasket.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  258 => 74,  255 => 73,  247 => 71,  239 => 69,  236 => 68,  234 => 67,  229 => 65,  224 => 62,  215 => 60,  213 => 59,  205 => 56,  201 => 55,  197 => 54,  192 => 51,  177 => 49,  173 => 48,  168 => 46,  163 => 44,  159 => 42,  155 => 41,  150 => 39,  146 => 38,  142 => 37,  138 => 36,  134 => 35,  129 => 32,  125 => 30,  123 => 29,  120 => 28,  111 => 27,  89 => 15,  84 => 14,  75 => 13,  62 => 10,  53 => 9,  43 => 1,  41 => 7,  39 => 6,  37 => 5,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% trans_default_domain 'basket' %}

{% set active = 'matchs' %}
{% set totalHT = 0 %}
{% set totalTTC = 0 %}

{% block page_title %}
    {{ 'basket.list' | trans }}
{% endblock %}

{% block stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" href={{ asset(\"css/style_list.css\") }}>
    <style media=\"print\">
        table td {
            vertical-align: middle;
            background: #ffffff;
            border: 2px solid gray;
            border-right: 0;
            box-shadow: 0px 0px 0px 0px #eaeaea;
        }
    </style>

{% endblock %}
{% block content %}

    {% if basket is empty %}
        <h1>Votre panier est vide pour le moment.</h1>
    {% else %}
        <div id='tableView'>
            <table>
                <tr>
                    <th>{{ 'basket.name' | trans }}</th>
                    <th>{{ 'basket.players.number' | trans }}</th>
                    <th>{{ 'basket.price' | trans }}</th>
                    <th>{{ 'basket.total.ht.price' | trans }}</th>
                    <th>{{ 'basket.delete' | trans }}</th>
                </tr>
                {% for match in matches %}
                    <tr>

                        <td>{{ match.name }}</td>
                        <td>
                            <form action=\"{{ path('participate_action', {'id' : match.id}) }}\" method=\"get\">
                                <select class=\"span1\" name=\"qte\" onchange=\"this.form.submit()\">
                                    {% for i in match.minNbPlayer..match.maxNbPlayer %}
                                        <option value=\"{{ i }}\" {% if i == basket[match.id] %} selected=selected {% endif %}>{{ i }}</option>
                                    {% endfor %}
                                </select>
                            </form>
                        </td>
                        <td>{{ match.game.price }}</td>
                        <td>{{ match.game.price * basket[match.id] }} €</td>
                        <td><a href=\"{{ path('basket_delete', {'id' : match.id}) }}\">{{ 'basket.delete' | trans }}</a>
                        </td>
                    </tr>
                    {% set totalHT = totalHT + (match.game.price * basket[match.id]) %}
                    {{ app.session.set('totalHT', totalHT) }}
                {% endfor %}
            </table>
            <dl class=\"pull-right\">
                <dt>Total :</dt>
                <dd>{{ totalHT }}</dd>
            </dl>
            {% if basket is not empty %}
                {% if is_granted('IS_AUTHENTICATED_FULLY') %}
                    <a href=\"{{ path('payment_form_action') }}\">{{ 'basket.payment' | trans }}</a>
                {% else %}
                    <a href=\"{{ url('login') }}\">{{ 'basket.payment' | trans }}</a>
                {% endif %}
            {% endif %}
        </div>
    {% endif %}
{% endblock %}
", "basket/userBasket.html.twig", "/Applications/MAMP/htdocs/shinigami_laser/templates/basket/userBasket.html.twig");
    }
}
