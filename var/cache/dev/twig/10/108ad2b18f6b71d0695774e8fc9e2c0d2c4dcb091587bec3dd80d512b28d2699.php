<?php

/* security/login.html.twig */
class __TwigTemplate_c13735e1b435fbcbfb07f4db72ffedf60e96a85e6c63418fcde63cfd17606783 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "security/login.html.twig", 1);
        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        // line 5
        $context["active"] = "login";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 7
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security.login", array(), "security"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 12
        echo "
    <div class=\"form\">
        <ul class=\"tab-group\">
            <li class=\"tab active\"><a href=\"#signup\">";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("security.login", array(), "security"), "html", null, true);
        echo "</a></li>
        </ul>
        ";
        // line 17
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 17, $this->source); })())) {
            // line 18
            echo "            <div>
                <i aria-hidden=\"true\"></i>
                ";
            // line 20
            echo twig_escape_filter($this->env, twig_replace_filter($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 20, $this->source); })()), "messageKey", array()), twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 20, $this->source); })()), "messageData", array()), "security"), array("Invalid credentials." => " Identifiants et/ou mot de passe incorrect.")), "html", null, true);
            echo "
            </div>


        ";
        }
        // line 25
        echo "
        ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 26, $this->source); })()), "flashes", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 27
            echo "            <div>
                ";
            // line 28
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "
        <form action=\"";
        // line 32
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("login");
        echo "\" method=\"POST\">
            ";
        // line 34
        echo "
            ";
        // line 35
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 35, $this->source); })()), "request", array()), "get", array(0 => "register"), "method") == "actived")) {
            // line 36
            echo "                <div class=\"alert alert-success\">
                    <p class=\"text-center\"><i class=\"fa fa-thumbs-up fa-2x\" aria-hidden=\"true\"></i></p>
                    <p class=\"text-center\">Le compte est déjà activé.</p>
                </div>
            ";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 40
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 40, $this->source); })()), "request", array()), "get", array(0 => "register"), "method") == "notfound")) {
            // line 41
            echo "                <div class=\"alert alert-danger\">
                    <p class=\"text-center\"><i class=\"fa fa-thumbs-down fa-2x\" aria-hidden=\"true\"></i></p>
                    <p class=\"text-center\">Cette email n'est pas valide.</p>
                </div>
            ";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 45
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 45, $this->source); })()), "request", array()), "get", array(0 => "register"), "method") == "banned")) {
            // line 46
            echo "                <div class=\"alert alert-danger\">
                    <p class=\"text-center\"><i class=\"fa fa-thumbs-down fa-2x\" aria-hidden=\"true\"></i></p>
                    <p class=\"text-center\">Le compte est banné.</p>
                </div>
            ";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 50
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 50, $this->source); })()), "request", array()), "get", array(0 => "register"), "method") == "closed")) {
            // line 51
            echo "                <div class=\"alert alert-danger\">
                    <p class=\"text-center\"><i class=\"fa fa-thumbs-down fa-2x\" aria-hidden=\"true\"></i></p>
                    <p class=\"text-center\">Ce compte est fermé.</p>
                </div>
            ";
        }
        // line 56
        echo "            <div>
                <div>
                    <input name=\"_username\" id=\"EMAILAUTEUR\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, (isset($context["last_email"]) || array_key_exists("last_email", $context) ? $context["last_email"] : (function () { throw new Twig_Error_Runtime('Variable "last_email" does not exist.', 58, $this->source); })()), "html", null, true);
        echo "\" required type=\"email\"
                           placeholder=\"Saisissez votre Email\">
                </div>
                <div>
                    <input name=\"_password\" id=\"MDPAUTEUR\" required type=\"password\" placeholder=\"*******\">
                </div>

            </div>
            <input type=\"hidden\" name=\"_target_path\" value=\"/accueil\"/>
            ";
        // line 68
        echo "            <input type=\"submit\" value=\"Connexion\">
            <div>
                <div>
                    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" checked />
                    <label for=\"remember_me\">Se souvenir de moi</label>

                </div>
                <a href=\"";
        // line 75
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("send_mail");
        echo "\">Mot de passe oublié</a>
            </div>
        </form>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 75,  181 => 68,  169 => 58,  165 => 56,  158 => 51,  156 => 50,  150 => 46,  148 => 45,  142 => 41,  140 => 40,  134 => 36,  132 => 35,  129 => 34,  125 => 32,  122 => 31,  113 => 28,  110 => 27,  106 => 26,  103 => 25,  95 => 20,  91 => 18,  89 => 17,  84 => 15,  79 => 12,  70 => 11,  57 => 8,  48 => 7,  38 => 1,  36 => 5,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% trans_default_domain 'security' %}

{% set active = 'login' %}

{% block page_title %}
    {{ 'security.login' | trans }}
{% endblock %}

{% block content %}

    <div class=\"form\">
        <ul class=\"tab-group\">
            <li class=\"tab active\"><a href=\"#signup\">{{ 'security.login' | trans }}</a></li>
        </ul>
        {% if error %}
            <div>
                <i aria-hidden=\"true\"></i>
                {{ error.messageKey|trans(error.messageData, 'security')| replace({\"Invalid credentials.\" : \" Identifiants et/ou mot de passe incorrect.\"}) }}
            </div>


        {% endif %}

        {% for message in app.flashes('success') %}
            <div>
                {{ message }}
            </div>
        {% endfor %}

        <form action=\"{{ url('login') }}\" method=\"POST\">
            {# Email et Mot de Passe #}

            {% if  app.request.get('register')==\"actived\" %}
                <div class=\"alert alert-success\">
                    <p class=\"text-center\"><i class=\"fa fa-thumbs-up fa-2x\" aria-hidden=\"true\"></i></p>
                    <p class=\"text-center\">Le compte est déjà activé.</p>
                </div>
            {% elseif  app.request.get('register')==\"notfound\" %}
                <div class=\"alert alert-danger\">
                    <p class=\"text-center\"><i class=\"fa fa-thumbs-down fa-2x\" aria-hidden=\"true\"></i></p>
                    <p class=\"text-center\">Cette email n'est pas valide.</p>
                </div>
            {% elseif  app.request.get('register')==\"banned\" %}
                <div class=\"alert alert-danger\">
                    <p class=\"text-center\"><i class=\"fa fa-thumbs-down fa-2x\" aria-hidden=\"true\"></i></p>
                    <p class=\"text-center\">Le compte est banné.</p>
                </div>
            {% elseif  app.request.get('register')==\"closed\" %}
                <div class=\"alert alert-danger\">
                    <p class=\"text-center\"><i class=\"fa fa-thumbs-down fa-2x\" aria-hidden=\"true\"></i></p>
                    <p class=\"text-center\">Ce compte est fermé.</p>
                </div>
            {% endif %}
            <div>
                <div>
                    <input name=\"_username\" id=\"EMAILAUTEUR\" value=\"{{ last_email }}\" required type=\"email\"
                           placeholder=\"Saisissez votre Email\">
                </div>
                <div>
                    <input name=\"_password\" id=\"MDPAUTEUR\" required type=\"password\" placeholder=\"*******\">
                </div>

            </div>
            <input type=\"hidden\" name=\"_target_path\" value=\"/accueil\"/>
            {# Bouton Submit #}
            <input type=\"submit\" value=\"Connexion\">
            <div>
                <div>
                    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" checked />
                    <label for=\"remember_me\">Se souvenir de moi</label>

                </div>
                <a href=\"{{ url('send_mail') }}\">Mot de passe oublié</a>
            </div>
        </form>
    </div>

{% endblock %}



", "security/login.html.twig", "/Applications/MAMP/htdocs/shinigami_laser/templates/security/login.html.twig");
    }
}
