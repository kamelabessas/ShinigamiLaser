<?php

/* match/show_match.html.twig */
class __TwigTemplate_d71722e67e237f6c5f863ee04a43ffc0aebffc1b73ed167c79accf89fd8c46cc extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("layout.html.twig", "match/show_match.html.twig", 1);
        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "match/show_match.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "match/show_match.html.twig"));

        // line 5
        $context["active"] = "establishment";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 7
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("match.show.establishment", array(), "match"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 12
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 15
        echo "
    <div class=\"row\">
        <!--colleft-->
        <div class=\"col-md-12 col-sm-12\">
            <section id=\"single-product\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-5\">
                            <div class=\"single-product-img\">
                                ";
        // line 24
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["match"]) || array_key_exists("match", $context) ? $context["match"] : (function () { throw new Twig_Error_Runtime('Variable "match" does not exist.', 24, $this->source); })()), "game", array()), "img", array()))) {
            // line 25
            echo "                                    <img alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["match"]) || array_key_exists("match", $context) ? $context["match"] : (function () { throw new Twig_Error_Runtime('Variable "match" does not exist.', 25, $this->source); })()), "game", array()), "name", array()), "html", null, true);
            echo "\" src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("images/game/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["match"]) || array_key_exists("match", $context) ? $context["match"] : (function () { throw new Twig_Error_Runtime('Variable "match" does not exist.', 25, $this->source); })()), "game", array()), "img", array()))), "html", null, true);
            echo "\" width=\"200\">
                                ";
        }
        // line 27
        echo "                            </div>
                        </div> <!-- End of /.col-md-5 -->
                        <div class=\"col-md-4\">
                            <div class=\"block\">
                                <div class=\"product-des\">
                                    <h4>";
        // line 32
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["match"]) || array_key_exists("match", $context) ? $context["match"] : (function () { throw new Twig_Error_Runtime('Variable "match" does not exist.', 32, $this->source); })()), "name", array()), "html", null, true);
        echo "</h4>
                                    <p class=\"price\">";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("match.price", array(), "match"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (" " . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["match"]) || array_key_exists("match", $context) ? $context["match"] : (function () { throw new Twig_Error_Runtime('Variable "match" does not exist.', 33, $this->source); })()), "game", array()), "price", array())), "html", null, true);
        echo " €</p>
                                    <p>";
        // line 34
        echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["match"]) || array_key_exists("match", $context) ? $context["match"] : (function () { throw new Twig_Error_Runtime('Variable "match" does not exist.', 34, $this->source); })()), "game", array()), "description", array());
        echo "</p>
                                </div>    <!-- End of /.product-des -->
                            </div> <!-- End of /.block -->

                            <p class=\"txt-color\">";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("match.select.players.number", array(), "match"), "html", null, true);
        echo " :</p>
                            <form action=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("participate_action", array("id" => twig_get_attribute($this->env, $this->source, (isset($context["match"]) || array_key_exists("match", $context) ? $context["match"] : (function () { throw new Twig_Error_Runtime('Variable "match" does not exist.', 39, $this->source); })()), "id", array()))), "html", null, true);
        echo "\" method=\"get\">
                                <div class=\"form-group\">
                                    <div class=\"col-lg-6\">
                                        <select name=\"qte\" class=\"form-control\">
                                            ";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(twig_get_attribute($this->env, $this->source, (isset($context["match"]) || array_key_exists("match", $context) ? $context["match"] : (function () { throw new Twig_Error_Runtime('Variable "match" does not exist.', 43, $this->source); })()), "minNbPlayer", array()), twig_get_attribute($this->env, $this->source, (isset($context["match"]) || array_key_exists("match", $context) ? $context["match"] : (function () { throw new Twig_Error_Runtime('Variable "match" does not exist.', 43, $this->source); })()), "maxNbPlayer", array())));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 44
            echo "                                                <option value=\"";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "</option>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "                                        </select>
                                    </div>
                                    <div class=\"col-lg-4\">
                                        <button class=\"btn btn-default\" id=\"add_match\">";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("match.add", array(), "match"), "html", null, true);
        echo "</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>    <!-- End of /.col-md-4 -->
                </div>    <!-- End of /.row -->
            </section> <!-- End of /.Single-product -->
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "match/show_match.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 49,  173 => 46,  162 => 44,  158 => 43,  151 => 39,  147 => 38,  140 => 34,  134 => 33,  130 => 32,  123 => 27,  115 => 25,  113 => 24,  102 => 15,  93 => 14,  80 => 12,  71 => 11,  58 => 8,  49 => 7,  39 => 1,  37 => 5,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'layout.html.twig' %}

{% trans_default_domain 'match' %}

{% set active = 'establishment' %}

{% block page_title %}
    {{ 'match.show.establishment' | trans }}
{% endblock %}

{% block stylesheets %}
    {{ parent() }}
{% endblock %}
{% block content %}

    <div class=\"row\">
        <!--colleft-->
        <div class=\"col-md-12 col-sm-12\">
            <section id=\"single-product\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-md-5\">
                            <div class=\"single-product-img\">
                                {% if match.game.img is not empty %}
                                    <img alt=\"{{ match.game.name }}\" src=\"{{ asset('images/game/'~match.game.img) }}\" width=\"200\">
                                {% endif %}
                            </div>
                        </div> <!-- End of /.col-md-5 -->
                        <div class=\"col-md-4\">
                            <div class=\"block\">
                                <div class=\"product-des\">
                                    <h4>{{ match.name }}</h4>
                                    <p class=\"price\">{{ \"match.price\" | trans() }} {{ \" \"~match.game.price }} €</p>
                                    <p>{{ match.game.description | raw }}</p>
                                </div>    <!-- End of /.product-des -->
                            </div> <!-- End of /.block -->

                            <p class=\"txt-color\">{{ 'match.select.players.number' | trans() }} :</p>
                            <form action=\"{{ path('participate_action', {'id':match.id}) }}\" method=\"get\">
                                <div class=\"form-group\">
                                    <div class=\"col-lg-6\">
                                        <select name=\"qte\" class=\"form-control\">
                                            {% for i in match.minNbPlayer..match.maxNbPlayer %}
                                                <option value=\"{{ i }}\">{{ i }}</option>
                                            {% endfor %}
                                        </select>
                                    </div>
                                    <div class=\"col-lg-4\">
                                        <button class=\"btn btn-default\" id=\"add_match\">{{ 'match.add' | trans() }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>    <!-- End of /.col-md-4 -->
                </div>    <!-- End of /.row -->
            </section> <!-- End of /.Single-product -->
        </div>
    </div>
{% endblock %}", "match/show_match.html.twig", "/Applications/MAMP/htdocs/shinigami_laser/templates/match/show_match.html.twig");
    }
}
