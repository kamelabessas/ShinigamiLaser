<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        // send_mail
        if ('/send-mail' === $pathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\Security\\ChangePasswordController::sendMailAction',  '_route' => 'send_mail',);
            if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                $allow = array_merge($allow, array('GET', 'POST'));
                goto not_send_mail;
            }

            return $ret;
        }
        not_send_mail:

        if (0 === strpos($pathinfo, '/c')) {
            // change_password
            if (0 === strpos($pathinfo, '/change-password') && preg_match('#^/change\\-password/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'change_password')), array (  '_controller' => 'App\\Controller\\Security\\ChangePasswordController::changePasswordAction',));
            }

            // check_email
            if ('/check-email' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\Security\\ChangePasswordController::checkEmail',  '_route' => 'check_email',);
            }

            if (0 === strpos($pathinfo, '/card')) {
                // list_cards
                if ('/card/list-cards' === $pathinfo) {
                    return array (  '_controller' => 'App\\Controller\\ShiniGami\\Card\\CardController::listCards',  '_route' => 'list_cards',);
                }

                // show_card
                if (0 === strpos($pathinfo, '/card/show-card') && preg_match('#^/card/show\\-card/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'show_card')), array (  '_controller' => 'App\\Controller\\ShiniGami\\Card\\CardController::showCard',));
                }

                // create_card
                if (0 === strpos($pathinfo, '/card/create-card') && preg_match('#^/card/create\\-card/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'create_card')), array (  '_controller' => 'App\\Controller\\ShiniGami\\Card\\CardController::createCard',));
                }

                // edit_card
                if (0 === strpos($pathinfo, '/card/edit-card') && preg_match('#^/card/edit\\-card/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_card')), array (  '_controller' => 'App\\Controller\\ShiniGami\\Card\\CardController::editCard',));
                }

                // delete_card
                if (0 === strpos($pathinfo, '/card/delete-card') && preg_match('#^/card/delete\\-card/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_card')), array (  '_controller' => 'App\\Controller\\ShiniGami\\Card\\CardController::deleteCard',));
                    if (!in_array($requestMethod, array('POST'))) {
                        $allow = array_merge($allow, array('POST'));
                        goto not_delete_card;
                    }

                    return $ret;
                }
                not_delete_card:

            }

        }

        // register
        if ('/register' === $pathinfo) {
            $ret = array (  '_controller' => 'App\\Controller\\Security\\SecurityController::register',  '_route' => 'register',);
            if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                $allow = array_merge($allow, array('GET', 'POST'));
                goto not_register;
            }

            return $ret;
        }
        not_register:

        // activate-account
        if (0 === strpos($pathinfo, '/activate-account') && preg_match('#^/activate\\-account/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'activate-account')), array (  '_controller' => 'App\\Controller\\Security\\SecurityController::activateAccount',));
        }

        // login
        if ('/login' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\Security\\SecurityController::login',  '_route' => 'login',);
        }

        if (0 === strpos($pathinfo, '/user')) {
            // list_users
            if ('/user/list-users' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\Security\\UserController::listUsers',  '_route' => 'list_users',);
            }

            // show_user
            if (0 === strpos($pathinfo, '/user/show-user') && preg_match('#^/user/show\\-user/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'show_user')), array (  '_controller' => 'App\\Controller\\Security\\UserController::showUser',));
            }

            if (0 === strpos($pathinfo, '/user/edit-user')) {
                // edit_user
                if (preg_match('#^/user/edit\\-user/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_user')), array (  '_controller' => 'App\\Controller\\Security\\UserController::editUserByUser',));
                }

                // edit_user_by_admin
                if (0 === strpos($pathinfo, '/user/edit-user-by-admin') && preg_match('#^/user/edit\\-user\\-by\\-admin/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_user_by_admin')), array (  '_controller' => 'App\\Controller\\Security\\UserController::editUserByAdmin',));
                }

            }

            // delete_user
            if (0 === strpos($pathinfo, '/user/delete-user') && preg_match('#^/user/delete\\-user/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_user')), array (  '_controller' => 'App\\Controller\\Security\\UserController::deleteUser',));
                if (!in_array($requestMethod, array('POST'))) {
                    $allow = array_merge($allow, array('POST'));
                    goto not_delete_user;
                }

                return $ret;
            }
            not_delete_user:

        }

        // participate_action
        if (0 === strpos($pathinfo, '/participate') && preg_match('#^/participate/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'participate_action')), array (  '_controller' => 'App\\Controller\\ShiniGami\\BasketController::participateAction',));
        }

        if (0 === strpos($pathinfo, '/basket')) {
            // basket_action
            if ('/basket' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\ShiniGami\\BasketController::basketAction',  '_route' => 'basket_action',);
            }

            // basket_delete
            if (0 === strpos($pathinfo, '/basket-delete') && preg_match('#^/basket\\-delete/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'basket_delete')), array (  '_controller' => 'App\\Controller\\ShiniGami\\BasketController::deleteAction',));
            }

            // payment_form_action
            if ('/basket/payment-form' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\ShiniGami\\BasketController::paymentFormAction',  '_route' => 'payment_form_action',);
            }

        }

        // validation_action
        if ('/validation' === $pathinfo) {
            return array (  '_controller' => 'App\\Controller\\ShiniGami\\BasketController::basketValidationAction',  '_route' => 'validation_action',);
        }

        // index_establishment
        if (preg_match('#^/(?P<_locale>[^/]++)/club$#sD', $pathinfo, $matches)) {
            $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'index_establishment')), array (  '_locale' => 'fr',  '_controller' => 'App\\Controller\\ShiniGami\\ClubController::indexAction',));
            if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                $allow = array_merge($allow, array('GET', 'POST'));
                goto not_index_establishment;
            }

            return $ret;
        }
        not_index_establishment:

        if (0 === strpos($pathinfo, '/club')) {
            // show_establishment
            if (0 === strpos($pathinfo, '/club/show-establishment') && preg_match('#^/club/show\\-establishment/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'show_establishment')), array (  '_controller' => 'App\\Controller\\ShiniGami\\ClubController::showAction',));
            }

            // add_establishment
            if ('/club/add-establishment' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\ShiniGami\\ClubController::addAction',  '_route' => 'add_establishment',);
            }

            // edit_establishment
            if (0 === strpos($pathinfo, '/club/edit-establishment') && preg_match('#^/club/edit\\-establishment/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_establishment')), array (  '_controller' => 'App\\Controller\\ShiniGami\\ClubController::updateAction',));
            }

            // delete_establishment
            if (0 === strpos($pathinfo, '/club/delete-establishment') && preg_match('#^/club/delete\\-establishment/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_establishment')), array (  '_controller' => 'App\\Controller\\ShiniGami\\ClubController::deleteAction',));
                if (!in_array($requestMethod, array('POST'))) {
                    $allow = array_merge($allow, array('POST'));
                    goto not_delete_establishment;
                }

                return $ret;
            }
            not_delete_establishment:

        }

        elseif (0 === strpos($pathinfo, '/game')) {
            // index_game
            if ('/game' === $pathinfo) {
                $ret = array (  '_controller' => 'App\\Controller\\ShiniGami\\GameController::indexAction',  '_route' => 'index_game',);
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_index_game;
                }

                return $ret;
            }
            not_index_game:

            // show_game
            if (0 === strpos($pathinfo, '/game/show-game') && preg_match('#^/game/show\\-game/(?P<slug>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'show_game')), array (  '_controller' => 'App\\Controller\\ShiniGami\\GameController::showAction',));
            }

            // add_game
            if ('/game/add-game' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\ShiniGami\\GameController::addAction',  '_route' => 'add_game',);
            }

            // edit_game
            if (0 === strpos($pathinfo, '/game/edit-game') && preg_match('#^/game/edit\\-game/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_game')), array (  '_controller' => 'App\\Controller\\ShiniGami\\GameController::updateAction',));
            }

            // delete_game
            if (0 === strpos($pathinfo, '/game/delete-game') && preg_match('#^/game/delete\\-game/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_game')), array (  '_controller' => 'App\\Controller\\ShiniGami\\GameController::deleteAction',));
                if (!in_array($requestMethod, array('POST'))) {
                    $allow = array_merge($allow, array('POST'));
                    goto not_delete_game;
                }

                return $ret;
            }
            not_delete_game:

        }

        // setlocale
        if (0 === strpos($pathinfo, '/setlocale') && preg_match('#^/setlocale(?:/(?P<language>[^/]++))?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'setlocale')), array (  'language' => NULL,  '_controller' => 'App\\Controller\\ShiniGami\\Language::setLocaleAction',));
        }

        if (0 === strpos($pathinfo, '/match')) {
            // index_match
            if ('/match' === $pathinfo) {
                $ret = array (  '_controller' => 'App\\Controller\\ShiniGami\\MatchController::indexAction',  '_route' => 'index_match',);
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_index_match;
                }

                return $ret;
            }
            not_index_match:

            // show_match
            if (0 === strpos($pathinfo, '/match/show-match') && preg_match('#^/match/show\\-match/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'show_match')), array (  '_controller' => 'App\\Controller\\ShiniGami\\MatchController::showAction',));
            }

            // add_match
            if ('/match/add-match' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\ShiniGami\\MatchController::addAction',  '_route' => 'add_match',);
            }

            // edit_match
            if (0 === strpos($pathinfo, '/match/edit-match') && preg_match('#^/match/edit\\-match/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_match')), array (  '_controller' => 'App\\Controller\\ShiniGami\\MatchController::updateAction',));
            }

            // delete_match
            if (0 === strpos($pathinfo, '/match/delete-match') && preg_match('#^/match/delete\\-match/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                $ret = $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_match')), array (  '_controller' => 'App\\Controller\\ShiniGami\\MatchController::deleteAction',));
                if (!in_array($requestMethod, array('POST'))) {
                    $allow = array_merge($allow, array('POST'));
                    goto not_delete_match;
                }

                return $ret;
            }
            not_delete_match:

            // list_scores_action
            if (0 === strpos($pathinfo, '/match/list-scores') && preg_match('#^/match/list\\-scores/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'list_scores_action')), array (  '_controller' => 'App\\Controller\\ShiniGami\\MatchController::listScoresAction',));
            }

            // matches_to_play_action
            if ('/match/matches-to-play' === $pathinfo) {
                return array (  '_controller' => 'App\\Controller\\ShiniGami\\MatchController::listMatchesToPlay',  '_route' => 'matches_to_play_action',);
            }

        }

        // index
        if (preg_match('#^/(?P<_locale>[^/]++)?$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'index')), array (  '_locale' => 'fr',  '_controller' => 'App\\Controller\\ShiniGami\\indexController::indexAction',));
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // accueil
        if ('/accueil' === $pathinfo) {
            return array (  'route' => 'index',  'permanent' => true,  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  '_route' => 'accueil',);
        }

        // deconnexion
        if ('/deconnexion' === $pathinfo) {
            return array('_route' => 'deconnexion');
        }

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
