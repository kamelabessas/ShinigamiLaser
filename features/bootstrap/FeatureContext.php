<?php

use App\Entity\Club;
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelDictionary;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements Context
{
    use KernelDictionary;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When I wait :duration
     */
    public function iWait($duration)
    {
        $this->getSession()->wait($duration * 1000);
    }

    /**
     * @Then Club :name should be in database
     */
    public function clubShouldBeInDatabase($name)
    {
        if (null === $this->getContainer()->get('doctrine')->getRepository(Club::class)->findOneByEstablishmentName($name))
        {
            throw new \Exception('L\'établissement n\'a pas été ajouté');
        }
    }
}
