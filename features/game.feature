Feature: Game page is accessible to all
  In order to access to Game page
  As a visitor
  I need to see game page via my browser

  @javascript
  Scenario: I can see game page
    Given I am on "game"
    When I wait "1"
    Then I should see "All rights reserved"

  @javascript
  Scenario: I can add game
    Given I am on "/match/show-match/1"
    When I press "add_match"
    Then I should see "Panier payment"


  @javascript
  Scenario: Add a game with success
    Given I am on "/club/add-establishment"
    When I fill in "club_establishmentName" with "Un établissement"
    And I wait "2"
    And I fill in "club_address" with "Une adresse"
    And I wait "2"
    And I press "Ajouter un club"
    And I wait "2"
    Then I should see "L'établissement 'Un établissement' a bien été ajouté"
    And Club "Un établissement" should be in database