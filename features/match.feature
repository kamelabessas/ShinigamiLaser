Feature: Match page is accessible to all
  In order to access to Match page
  As a visitor
  I need to see match page via my browser

  @javascript
  Scenario: I can see match page
    Given I am on "match"
    When I wait "1"
    Then I should see "All rights reserved"

  @javascript
  Scenario: I can add game
    Given I am on "/match/show-match/1"
    When I press "add_match"
    Then I should see "Panier payment"


  @javascript
  Scenario: Add a game with success
    Given I am on "/club/add-establishment"
    When I fill in "club_establishmentName" with "Un établissement"
    And I wait "2"
    And I fill in "club_address" with "Une adresse"
    And I wait "2"
    And I press "Club add"
    And I wait "2"
    Then I should see "L'établissement 'Un établissement' a bien été ajouté"
    And Club "Un établissement" should be in database