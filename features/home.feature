Feature: My project is accessible
  In order to access to my projet
  As a visitor
  I need to see pages via my browser

  @javascript
  Scenario: I can see Homepage
    Given I am on "/"
    When I wait "1"
    Then I should see "accueil"